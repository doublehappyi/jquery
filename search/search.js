//创建类
;!( function ( global,factory ) {
	if( typeof exports == 'object' && exports ){
		factory( exports );
	} else if( typeof define == 'function' && define.amd ){
		define( ['exports'],factory );
	} else {
		factory( global.Alpha = {} );
	}
})( Function('return this')(),function ( Alpha ) {
	'use strict';
	var global = Function('return this')(),
		_object_ = {},
		_array_ = [],
		_hasOwn_ = _object_.hasOwnProperty,
		_toStr_ = _object_.toString,
		_slice_ = _array_.slice,
		_fn_ = Function,
		undefined,
		cls;

	Alpha.indexOf = _array_.indexOf ?
		function( array,value ){
			return array.indexOf( value )
		} : function( array,value ){
		var len = array.length;

		while( len-- ){
			if( array[len] === value ){
				return len;
			}
		}
		return -1;
	}

	Alpha.extend = function( tar,src ){
		var hOwn = _hasOwn_,
			len = arguments.length,
			i = 0;

		tar = arguments[i++];

		while( i < len ){
			src = arguments[ i ];
			for( var key in src ){
				if( hOwn.call( src,key ) ){
					tar[ key ] = src[ key ];
				}
			}
			i++;
		}

		return tar;
	}

    //返回一个对象，
    //该对象继承自parent：如果parent有prototype属性，则继承其prototype（注意，这里的prototype不一定是函数的prototype属性，可以是对象的自定义属性），否则，直接继承parent
    Alpha.inhert = function( name,parent ){
		var Empty = Function('return function '+ name +'(){}')();
		Empty.prototype = parent.prototype || parent;

		return new Empty;
	}

    //返回一个延时执行函数
	Alpha.throttle = function throttle( callback,t,ctx ){
		var handle,
			timer;

		handle = function(){
			var args = arguments;

			clearTimeout( timer );
			timer = global.setTimeout(function(){
				global.clearTimeout( timer );
				callback.apply( ctx,args );
			},t || 200);
		}

		handle._expo ="throttle_function";

		return handle;
	}

	Alpha.isLikeArray = function( object ){
		var len = object.length;

		if( len === undefined ){
			return false;
		}

		if(  len > 0 && object[ len - 1 ] in object ){
			return true;
		}
		return false;
	}

	Alpha.map = _fn_( 'return ' +
		_forEachTpl_( {
			variable: 'ret = []',
			statment: 'ret.push( value )',
			ret: 'return ret'
		} )
	)();

	Alpha.each = _fn_( 'return ' +
		_forEachTpl_( {
			statment: 'if( value === false ){ return; }'
		} )
	)();

	Alpha.some = _fn_( 'return ' +
		_forEachTpl_( {
			statment: 'if( value === true ){ return true }'
		} )
	)();

	Alpha.filter =  _fn_( 'return ' +
		_forEachTpl_( {
			variable: 'ret = []',
			statment: 'if( value === true ){ ret.push( list[i] ) }',
			ret: 'return ret'
		} )
	)();


	Alpha.Class = cls = {
		factory: function (name, parent) {
			var cls,
				fn,
				hOwn = _hasOwn_,
				inhert = Alpha.inhert,
				extend = Alpha.extend;

			if( parent === undefined ){
				parent = name;
				name = undefined;
			}

			if (name === undefined) {
				name = 'Anonymous';
			}

			cls = {
				factory: function () {
					var instance = inhert(name, this.prototype);

					instance.initialize.apply(instance, arguments);
					return instance;
				},
				extend: function (factory) {
					var proto;

					if (factory === undefined) {
						return this;
					}

					switch (typeof factory) {
						case 'undefined':
							return this;
						case 'function':
							proto = factory(fn, parent);
							if (proto == fn) {
								return this;
							}
							break;
						default:
							proto = factory;
					}
					extend(fn, proto);

					return this;
				},
				prototype: parent ? fn = inhert(name, parent) : fn = {}
			};

			cls.fn = fn;

			return cls;
		},
		extend: Alpha.extend,
		inhert: Alpha.inhert
	};

	Alpha.Data = cls.factory('Data')
		.extend({
			initialize: function(){
				this.dataCache = {};
				this.dataHash = {};
				this.dataIndex = 0;
			},
			save: function( ns,data ){
				var cache = this.dataCache,
					hash = this.dataHash,
					index = this.dataIndex,
					handler;

				if( typeof ns == 'string' ){
					if( data ){
						handler = cache[ hash[ ns ] = index++ ] = data;
					}
				}

				this.dataIndex = index;

				return handler;
			},
			read: function( ns ){
				var cache = this.dataCache,
					hash = this.dataHash,
					handler;

				if( typeof ns == 'string' ) {
					handler = cache[ hash[ ns ] ];
				}

				return handler;
			},
			clear: function( ns ){
				var cache = this.dataCache,
					hash = this.dataHash,
					index,
					handler;

				if( ns ){
					index = hash[ ns ];
					handler = cache[ index ];
					if( handler ){
						delete cache[ index ];
						delete hash[ ns ];
					}
				}

				return this;
			},
			exist: function( ns ){
				return this.dataHash[ ns ];
			}
		});

	Alpha.Event = cls.factory('Event',Alpha.Data)
		.extend(function( fn,_super ){
			return {
				initialize: function( config ){
					_super.fn.initialize.call( this,'event' );
				},
				build: function( name ){

                    //功能：
                    // 1，当index为负数的时候，就是要从数组后面插入新的元素，最后一个元素是-1，倒数第二个元素是-2；依次类推
                    // 2，当index大于length的时候，将其修正至最后一个最后一个位置
                    // 3，如果一切正常，则不做任何处理
                    // 4，如果负数非常大，其绝对值甚至大于了length，这里并没有做处理。为了程序的健壮性，这里可以做进一步的处理。
                    // 亲测chrome 41.02272.89m版本，splice方法原生支持如下功能：如果第一个参数大于length，就按最后一个位置来算；如果为-1，则按照倒数第二个来算，依次类推
					function fixIdx( length,index ){
						return index < 0 ?
							length + index + 1 :
							index > length ?
								length : index;
					}

					return function( handle,index ){
						var cache;

						if( typeof handle == 'function' ){
							cache = this.read( name );

							if( !cache ){
								cache = this.save( name,[] );
							}

							index === undefined ?
								cache.push( handle ) :
								cache.splice( fixIdx(cache.length,index),0,handle );//这里的splice起一个往数组里面插入元素的作用
						}

						return this;
					}
				},
				emit: function( ns ){
					var cache = this.read( ns ),
						i = 0,
						ret,
						arg;

					if( cache ){
						arg = _slice_.call( arguments,1 );//这里会把emit传入的第二个及之后的参数截取下来
						while( i < cache.length ){
                            //对cache数组里面的每一个函数，都应用上emit函数传入的参数arg
							ret = cache[i++].apply( this,arg );
                            //任何一个函数返回false的话，就终止后续函数的执行
							if( ret === false ){
								break;
							}
						}
					}

					return this;
				},
				once: function( ns,handle ){
					var nHandle,
						self = this;

                    //这里等于是装饰器！！！
					nHandle = function(){
						handle.apply( self,arguments );
                        //函数执行完之后，马上解绑！
						self.off( ns,nHandle )
					}

                    //这里的return值，是this.on()函数执行之后的值，而this.on执行之后，刚刚好返回this
                    //其实这里我并不喜欢这样做，最好是先执行this.on()，然后再写一个return this;这样代码更清晰！耦合性也会稍微降低一点！
					return this.on( ns ,nHandle );
				},
				on:	function( ns,handle,index ){
					var method = this[ ns ];

					if( !method ){//？！为什么会在这里加个这样的判断，this[ns]应该 一直都不存在啊？
						method = this.build( ns );
					}

                    //这里把handle处理函数push到this.dataCache的数组里面去
					method.call( this,handle,index );

					return this;
				},
				off: function( ns,handle ){
					var cache = this.read( ns ),
						len;

					if( cache ){
						if( typeof handle == 'function' ){
							len = cache.length;

							while( len-- ){
								if( cache[ len ] == handle ){
									cache.splice( handle,len );

									if( cache.length < 1 ){
										this.clear( ns );
									}
									break;
								}
							}

						}
						else{
							this.clear( ns );
						}
					}
					return this;
				}
			}
		});

	Alpha.Counter = cls.factory('Counter',Alpha.Event)
		.extend(function(fn,_super){
			return {
				initialize: function( config ){
					var self = this;

					config = config || {};
					_super.fn.initialize.call( this,config );

					this.current = 1;
					this.total = config.total || 0;
					this.timeout = config.timeout || undefined;
					this.context = config.context || undefined;
					this.timer = null;
					this.state = 'ready';

					this.data = [];

					if( config.error ){
						this.fail(function(){
							config.error.apply( self,arguments );
						})
					}

					if( config.fail ){
						this.fail(function(){
							config.fail.apply( self,arguments );
						})
					}

					if( config.done ){
						this.done(function(){
							config.done.apply( self,arguments );
						})
					}

					if( config.success ){
						this.done(function(){
							config.success.apply( self,arguments );
						})
					}

					if( !this.total ){
						this.state = 'end';
					}

					this.config = config;

					if( config.auto ){
						this.watch();
					}
				},
				update: function( type,value ){
					if( this.state == 'end' ){
						return;
					}

					type = type || 'done';

					if( type == 'error' || type == 'fail' ){
						global.clearTimeout( this.timer );

						this.emit('fail',{
							code: '1',
							message: 'one of the tasks has a error',
							detail: value
						});
					}

					this.current++;

					if( this.current > this.total ){
						this.emit('done',this.data);
						this.state = 'end';
					}

					if( value ){
						this.data.push( value );
					}

					return this;
				},
				watch: function(){
					var self = this;

					if( this.timeout > 0 ){
						this.timer = global.setTimeout(function(){
							self.state = 'end';
							self.emit('fail',{
								code: '-1',
								message: 'time out'
							});
						},this.timeout);
					}

					return this;
				},
				done: fn.build('done'),
				fail: fn.build('fail')
			}
		});

	Alpha.Pipe = cls.factory('Pipe',Alpha.Event)
		.extend(function(fn,_super){
			return{
				initialize: function( config ){
					config = config || {};
					_super.fn.initialize.call( this,config );

					var isArr = _toStr_.call( config.source ) == '[object Array]';

					this.source = isArr ?
						config.source
						: typeof config.source == 'function' ?
							[config.source] : [];

					this.args = [];
					this.current = 0;

					if( config.error || config.fail ){
						this.on('fail',config.error || config.fail)
					}

					if( config.done || config.success ){
						this.on('done',config.done || config.success);
					}

					this.on('clear.pipe',function(){
						this.args = [];
						this.current = 0;
					});

				},
				inject: function( handle ){
					var len = arguments.length,
						item = this.source,
						slice = _slice_,
						arg,
						idx,
						self = this;

					if( len > 0 ){
						if( typeof handle == 'function' ){
							item.push( handle );
						}
					}
					else{
						idx = this.current++;
						handle = item[ idx ];

						if( !handle ){
							this.args.unshift('done');

							this.emit.apply( this,this.args );
							this.emit('clear.pipe');

							return this;
						}
						arg = slice.call( this.args,0 );

						arg.push({
							next: function(){
								self.args = slice.call( arguments );
								self.inject();
							},
							fail: function( detail ){
								self.emit('clear.pipe')
									.emit('fail',{
										index: idx,
										handle: handle,
										detail: detail
									});
							}
						});

						this.args = [];

						handle.apply( this,arg );
					}
					return this;
				}
			}
		});


	function _forEachTpl_( ret ){
		if( ret === undefined ){
			return;
		}

		return 'function( list,callback,context ' + ( ret.param ? ',' + ret.param : '' ) + ' ){'
			+ 'var i,len,value' + ( ret.variable ? ',' + ret.variable : '' ) + ';'
			+ 'if( Alpha.isLikeArray(list) ){'
			+ 'for( i= 0,len = list.length; i < len; i++  ){'
			+ 'value = callback.call( context || null, i ,list[i] );'
			+ ( ret.statment ? ret.statment : '' ) + ';'
			+ '}'
			+ '}'
			+ 'else{'
			+ 'for( i in list ){'
			+ 'value = callback.call( context,i,list[i] );'
			+ ( ret.statment ? ret.statment : '' )
			+ '}'
			+ '}'
			+ ( ret.ret ? ret.ret : '' )
			+ '}';
	}
});
;/**
 * Created by yan.Div on 2014/12/27.
 */
;!function( Alpha ){
    'use strict';
    var global = Function('return this')(),
        doc = document,
        cls = Alpha.Class,
        $ = Zepto,
        _object_ = {},
        _array_ = [],
        _slice_ = _array_.slice,
        _toStr_ = _object_.toString,
        _hasOwn_ = _object_.hasOwnProperty;

    Alpha.Ctrl = cls.factory('Ctrl',Alpha.Event)
        .extend(function(fn,_super){
            return {
                initialize: function( config ){
                    config = config || {};
                    _super.fn.initialize.call( this );

                    this._elem = null;
                    this.variable = {};

                    if( config.elem ){
                        this._elem = $( config.elem );

                        if( config.event ){
                            this.watch( config.event );
                        }

                        if( config.scan ){
                            this.scan();
                        }
                    }

                    this.config = config;
                },
                //給this.varible设置key-value键值对，然后返回this
                set: function( k,v ){
                    (Function('return function(value){ return arguments.length < 1 ? this.'+ k +' : this.'+ k +' = value;}')()).call( this.variable,v )

                    return this;
                },
                //返回this.varible中的键为k的值
                get: function( k ){
                    return (Function('return function(){ return this.'+ k +' ;}')()).call( this.variable )
                },
                //1，find参数可以有1个或多个，参数都是字符串类型
                //2，此函数查找this._item（其实就是Zepto对象，类数组类型，里面全是dom对象）里面的所有符合以下条件的dom元素：
                //其elem-point属性值等于find函数任意参数值的dom，然后将这些dom元素重新用$包裹起来！并返回此包裹之后的对象
                find: function(){
                    var item = this._item,
                        len,
                        hold,
                        ret = [];

                    len = arguments.length;

                    while( len-- ){
                        hold = arguments[ len ];
                        item.each(function(i,elem){
                            if( $( elem).attr('elem-point') == hold){
                                ret.push( elem );
                            }
                        });
                    }

                    return $( ret );
                },
                //该方法其实就是查询this._elem下面的所有具有属性为elem-point的dom元素，然后重新赋值到this._item
                //调用该scan方法之前，this._elem相当于一个容器，调用该scan方法之后，this._elem等于被换成了目标dom元素的Zepto对象
                scan: function( name ){
                    var elem;

                    if( this._elem ){
                        //如果this._elem的第一个元素值等于global或者等于doc，则elem=$(doc),否则，elem等于this._elem;
                        elem = this._elem.get(0) == global ||  this._elem.get(0) == doc ?
                            $( doc ) : this._elem;
                        //this._item被重新赋值，其值为elem下面拥有属性值["elem-point"]的所有dom元素组成的Zepto对象
                        this._item = elem.find('[elem-point]')
                    }

                    return this;
                },
                watch: function( evt ){
                    var evt = evt || this.event,
                    //匹配非空，至少1个的任意字符，后面可以跟随最多1个空格
                        rtype = /(^[\S]+)\s?/g,
                        hasOwn = _hasOwn_,
                        self = this,
                        handle,
                        select,
                        type,
                        k;

                    if( evt ){
                        for( k in evt ){
                            if( hasOwn.call( evt,k ) ){
                                (function( k,handle ){
                                    var select,
                                        type;

                                    //这里$2的值是匹配的rtype匹配的编号为1的子表达式，即第一个括号里面的表达式
                                    select = k.replace(rtype,function( $1,$2 ){
                                        type = $2;
                                        return '';
                                    });

                                    if( !type ){
                                        return;
                                    }

                                    this._elem.on( type,select,function( e ){
                                        handle.call( self,e,$(e.currentTarget ))
                                    });

                                }).call( this,k,evt[k] )

                            }
                        }
                    }

                    return this;
                }
            }
        });

}( Alpha );
;/**
 * Created by yan.Div on 2014/12/27.
 */
;!function( Alpha ){
    'use strict';
    var global = Function('return this')(),
        cls = Alpha.Class,
        extend = Alpha.extend,
        $ = Zepto;

    //Model主要提供了request功能,以及添加request.before等事件功能
    Alpha.Model = cls.factory('Model',Alpha.Event)
        .extend(function(fn,_super){
            return{
                initialize: function( config ){
                    config = config || {};
                    _super.fn.initialize.call( this );

                    this.transport = config.transport || $.ajax;
                    this.url = config.url || '';
                    this.dataType = config.dataType || 'json';
                    if( !(this.dataType == 'jsonp') ){
                        this.type = config.type || 'get';
                    }
                    this.param = config.param || {};

                    this.data = config.data;
                    this.config = config;

                    return this;
                },
                request: function( type,handle ){
                    var self = this;

                    switch(type ){
                        case 'before':
                            this.on('request.before',handle);
                            break;
                        case 'after':
                            this.on('request.after',handle);
                        case 'done':
                            this.on('request.done',handle);
                            break;
                        case 'fail':
                            this.on('request.fail',handle);
                            break;
                        default :
                            //如果没有配置url也没有配置data，那么直接返回
                            if( !this.url ){
                                if( !this.data ){
                                    return this;
                                }
                            }

                            //如果配置了data，那么就直接触发request.done事件，然后返回this（因为this.emit返回了this对象）
                            //从这里可以看出，data配置的优先级要高于url配置
                            if( this.data ){
                                //这里把this.data作为request.done回调函数的第一个参数传入，并且把'local'作为第二个参数传入回调函数
                                return this.emit('request.done',this.data,'local')
                            }

                            //如果配置了url，并且没有配置data
                            global.setTimeout(function(){
                                var that = self,
                                    data,
                                    //这里组装出zepto风格的ajax请求配置
                                    param = {
                                        url: that.url,
                                        dataType: that.dataType,
                                        success: function( ret,message,hold ){
                                            that.hold = null;
                                            that.emit('request.done',ret,message,hold);
                                        },
                                        error: function( err,message ){
                                            that.hold = null;
                                            that.emit('request.fail',err,message );
                                        }
                                    };

                                //请求方式的配置
                                if( this.type ){
                                    param.type = this.type;
                                }
                                //先触发ajax请求前的事件
                                that.emit('request.before',data = {});
                                //再发出ajax请求，参数用当前的param覆盖用户传入的param：从这里可以看出，用户不能传success之类的回调函数配置。
                                //其实这里可以做优化，url,dataType以及type之类的参数，都可以直接放到用户传入的config对象里面去进行配置就好了，然后让用户的配置覆盖掉默认的配置，
                                //甚至可以支持用户配置回调函数，这些用户配置的回调函数可以放到默认param里面去执行:that.param.success.call(that, ret, message, hold);
                                //这个hold是ajax请求的对象，随时可以被abort掉
                                that.hold = that.transport(extend(data,that.param,param));
                                //ajax请求之后，再触发request.after
                                that.emit('request.after');
                            },1);
                    }

                    return this;
                }
            }
        });

}( Alpha );
;/**
 * Created by yan.Div on 2014/12/27.
 */
;!function( Alpha ){
    'use strict';
    var cls = Alpha.Class,
        undefined,
        extend = Alpha.extend,
        $ = Zepto;

    Alpha.View = cls.factory('View',Alpha.Model)
        .extend(function(fn,_super){
            return{
                initialize: function( config ){
                    config = config || {};
                    _super.fn.initialize.call( this,config );

                    this.engine = config.engine || Mustache;
                    this.template = typeof config.template == 'object' ?
                        config.template.html()
                        : $( config.template ).html();


                    //this.variable对象的初始化在Alpha.Event的initialize方法中已经有了，这里还要初始化？
                    this.variable = {};
                    //this._elem对象的初始化在Alpha.Event的initialize方法中已经有了，这里还要初始化？
                    this._elem = $( config.elem );
                },
                //Alpha.Event对象已经有了set方法，为什么这里还要有这个方法？！
                set: function( k,v ){
                    (Function('return function(value){ return arguments.length < 1 ? this.'+ k +' : this.'+ k +' = value;}')()).call( this.variable,v )

                    return this;
                },
                //Alpha.Event对象已经有了get方法，为什么这里还要有这个方法？！
                get: function( k ){
                    return (Function('return function(){ return this.'+ k +' ;}')()).call( this.variable )
                },
                render: function( type, handle,render){
                    switch( type ){
                        case 'before':
                            this.on('render.before',handle);
                            break;
                        case 'format':
                            this.on('render.format',handle);
                            break;
                        case 'after':
                            this.on('render.after',handle);
                            break;
                        default:
                            !(function( ret,render,elem ){
                                var html,
                                    model,
                                    isFn = typeof ret == 'function';

                                //如果只传2个参数，并且第一个参数是个函数，那么第二个参数就必须是elem，render指向ret
                                //等于是第一个参数ret没有传，render和elem就上位了的意思
                                if( elem === undefined ){
                                    if(isFn){
                                        elem = render;
                                        render = ret;
                                        ret = undefined;
                                    }
                                }

                                //如果elem没有传，即只有1个参数的时候（该参数必须是render函数），则默认使用this._elem;
                                elem = elem || this._elem;
                                ret = ret || this.data;

                                //如果elem没有，即没有传elem并且this._elem也没有值，那么就什么也不做
                                if( elem ){
                                    //先后触发render.before和render.format事件
                                    this.emit('render.before',ret)
                                        .emit('render.format',ret);

                                    model = this.renderModel || ret;

                                    html =  this.engine.render( this.template,model ) ;

                                    //如果传了自定义的render函数，那么久调用自定义的render函数，否则，调用默认的render，直接把html拼接到elem上去
                                    render ?
                                        render.call(this,elem,html,model) : elem.append( html );

                                    //触发render.after事件
                                    this.emit('render.after',ret,html,model);
                                }


                            }).call( this,type,handle,render );
                    }
                    return this;
                },
                empty: function(){
                    this._elem.html('');
                    return this;
                },
                fresh: function(){
                    return this.render();
                }
            }
        });

}( Alpha );
;/**
 * Created by yan.Div on 2014/12/27.
 */
;!function( Alpha ){
    'use strict';
    var global = Function('return this')(),
        doc = document,
        cls = Alpha.Class,
        extend = Alpha.extend,
        $ = Zepto,
        _object_ = {},
        _hasOwn_ = _object_.hasOwnProperty,

        report_sort_table = {
            '6': 2,
            '7': 3,
            '24': 1,
            '21': 4,
            '77': 0
        },

        //引用
        ecc = global.ECC,
        ecc_report = ecc.cloud.report,
        ecc_data = ecc_report.data;

    Alpha.BaseReport = cls.factory('BaseReport',Alpha.Ctrl)
        .extend(function(fn,_super){
            return {
                initialize: function( config ){
                    config = config || {};
                    _super.fn.initialize.call( this,config );

                    this.baseParam = extend( config.param, this.parse( 'ptag',config.ptag ));

                    this.ptag = config.ptag;
                },
                parse: function( type, value ){
                    switch( type ){
                        case 'ptag':
                            return (function( value ){
                                var item = ['ptagPageid','ptagDomainid','ptagLinkid'],
                                    param = {},
                                    len = 3;

                                if( value ){
                                    value = value.split('.');
                                }

                                if( value && value.length > 0 ){
                                    param[ item[ --len ] ] = value[ len ];
                                    param[ item[ --len ] ] = value[ len ];
                                    param[ item[ --len ] ] = value[ len ];
                                }
                                else{
                                    param[ item[ --len ] ] = '';
                                    param[ item[ --len ] ] = '';
                                    param[ item[ --len ] ] = '';
                                }

                                return param;
                            }).call( this, value );
                            break;
                        case 'sort':
                            return report_sort_table[ value ];
                    }
                },
                transate: function( object ){
                    var ret = [],
                        key,
                        hasOwn = _hasOwn_;

                    for( key  in object){
                        if( hasOwn.call( object,key ) ){
                            ret.push( key + ':' + object[ key ] );
                        }
                    }

                    return ret.join('|');
                }
            }
        });

    Alpha.PVReport = cls.factory('PVReport',Alpha.BaseReport)
        .extend(function(fn,_super){
            return {
                initialize: function( config ){
                    config = config || {};
                    _super.fn.initialize.call( this,config );

                    if( config.auto ){
                        this.oldAuto = this.auto = config.auto;
                    }
                },
                report: function( type,args,force ){
                    switch( type ){
                        case 'site':
                            this.site( args,force );
                            break;
                        case 'search':
                            this.search( args,force );
                            break;
                        case 'expo':
                            this.expo( args,force );
                            break;
                    }
                    return this;
                },
                site: function( args,force ){
                    this.site = site;

                    if( force ){
                        this.site( args );
                    }

                    function site( args ){
                        ecc_data.u = encodeURIComponent( location.href + args );
                        ecc_report.setInfo();
                        ecc_data.p = 'paipai.com';
                        ecc_report.pv(FOOTDETECT.objMsgPv,true);
                        return this;
                    }

                    return this;
                },
                search: function( p,sce ){
                    var param = this.baseParam;

                    p.PVType = 0;
                    ecc_data.p = 's.wanggou.com';

                    if( sce ){
                        p = extend( {},p,sce );
                    }

                    ecc_report.pv( extend({},param,p),true );

                    return this;
                },
                expo: function( p,sce ){
                    var param = this.baseParam;

                    p.PVType = 1;
                    ecc_data.p = 's.wanggou.com';

                    if( sce ){
                        p = extend( {},p,sce );
                    }

                    ecc_report.pv( extend({},param,p),true );

                    return this;
                },
                parse: function( type,value ){
                    switch( type ){
                        case 'expo':
                            return (function( value ){
                                var len = value.length,
                                    point,
                                    position = [],
                                    classid = [],
                                    cid = [],
                                    type = [],
                                    extra = [],
                                    i = 0;

                                while( i < len ){
                                    point = value[ i ];
                                    position.push( i + 1 );
                                    classid.push( point.LeafClassId );
                                    cid.push( point.commId );
                                    extra = [];

                                    if( point.Active_Price_Type == '44' ){
                                        extra.push( 'ppy' );
                                    }

                                    if( point.Active_Price_Type == '43' ){
                                        extra.push( 'flash' );
                                    }

                                    if( point.PersonalizedTypeTag == '2' ){
                                        extra.push( 'view' )
                                    }

                                    if( point.PersonalizedTypeTag == '1' ){
                                        extra.push( 'buy' )
                                    }

                                    if( extra.length < 1 ){
                                        extra.push( 'o' );
                                    }

                                    type.push( extra.join( '~' ) );

                                    i++;
                                }

                                if( cid.length < 1 ){
                                    return {
                                        pos: '',
                                        classid: '',
                                        itemid: '',
                                        extraMsg: ''
                                    }
                                }

                                return {
                                    pos: position.join('_'),
                                    classid: classid.join('_'),
                                    itemid: cid.join('_'),
                                    extraMsg: type.join('_')
                                }

                            }).call(this,value);
                            break;
                        default:
                            return _super.fn.parse.apply( this,arguments );
                    }
                },
                pvid: function(){
                    return ecc_data.pvid;
                }
            }
        });

    Alpha.TapReport = cls.factory('TapReport',Alpha.PVReport)
        .extend(function(fn,_super){
            return {
                initialize: function (config) {
                    config = config || {};
                    _super.fn.initialize.call(this, config);

                    this.tapTable = {};
                },
                tapReport: function (data, tar, pvid) {
                    var e_report = ecc_report,
                        e_data = ecc_data;

                    if (pvid) {
                        e_data.pvid = pvid;
                    }

                    e_data.p = 's.wanggou.com';
                    e_report.trace(data, tar[0] || tar);
                },
                tap: function (ns, value) {
                    typeof ns == 'object' ?
                        extend(this.tapTable, ns) :
                        this.tapTable[ns] = value;

                    return this;
                },
                parse: function (type, value,serial) {
                    switch (type) {
                        case 'locid':
                            return (function (value) {
                                var len,
                                    i = 0,
                                    ret = {};

                                value = value.split('-');
                                len = value.length;

                                if (len > 0) {
                                    while (i < len) {
                                        ret['locid' + ( i + 1 )] = value[i] || '';
                                        i++;
                                    }
                                    return ret;
                                }
                            }).call(this, value);
                            break;
                        case 'tap':
                            return (function (value,serial ) {
                                var name = serial,
                                    ret = {},
                                    param = {},
                                    len = name.length,
                                    i = 0;

                                value = value.split(',');

                                if (value.length > 0) {

                                    while (i < len) {
                                        param[name[i]] = value[i];
                                        i++;
                                    }

                                    ret.param = param;
                                    return ret;
                                }

                            }).call(this, value,serial);
                            break;
                        default:
                            return _super.fn.parse.apply(this, arguments);
                    }
                }
            }
        });
    
}( Alpha );
;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha ){
    'use strict';
    var global = Function('return this')(),
        cls = Alpha.Class,
        undefined,
        extend = Alpha.extend,
        $ = Zepto;

    Alpha.Page = cls.factory('Page',Alpha.View)
        .extend(function( fn,_super ){
            return {
                initialize: function( config ){
                    config = config || {};
                    _super.fn.initialize.call( this,config );

                    this.current = 0;
                },
                next: function(){
                    if( this.get('locked') ){
                        return;
                    }

                    this.current++;

                    this.request();

                    return this;
                },
                reset: function(){
                    this.empty();
                    this.current = 0;
                    this.set('locked',false);

                    return this;
                }
            }
        });

}( Alpha );;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha ){
    'use strict';
    var global = Function('return this')(),
        cls = Alpha.Class,
        undefined,
        _object_ = {},
        _toStr_ = _object_.toString,
        extend = Alpha.extend,
        indexOf = Alpha.indexOf,
        $ = Zepto;

    Alpha.Filter = cls.factory('Filter',Alpha.Ctrl)
        .extend(function( fn,_super ){
            return {
                initialize: function( config ){
                    config = config || {};
                    _super.fn.initialize.call( this,config );

                    this.current = 0;
                },
                insert: function( key,value ){
                    var hold = this.get(key),
                        isArr = _toStr_.call( hold ) == '[object Array]';

                    isArr ?
                        hold.push( value ) : this.set(key,value);
                },
                remove: function( key,value ){
                    var hold = this.get(key),
                        isArr = _toStr_.call( hold ) == '[object Array]',
                        idx;

                    if( isArr ){
                        idx = indexOf( hold,value );
                        if( !(idx == -1) ){
                            hold.splice( idx,1 );
                        }
                    }
                    else{
                        this.set( key,undefined );
                    }

                    return this;
                },
                calculate: function(key,handle){
                    return handle ?
                        this.set(key + 'Calculate',handle) :
                        (handle = this.get(key + 'Calculate')) && handle.call( this );
                }
            }
        });

}( Alpha );;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha ){
    var global = Function('return this')(),
        Manager;

    global.Manager = Manager = Alpha.Ctrl.factory();

    Manager.set('config',{
        throttle: 50,
        threshold: 100,
        times: 2,
        piece: 2
    })
    .set('api',{
        comm: 'http://sse1.paipai.com/comm_json?ClassDirect=1',
        shop: 'http://ss.paipai.com/json/search',
        cate: 'http://ms.paipai.com/category',
        supp: 'http://api.search.jd.com/',
        word: 'http://ms.paipai.com/recoword',
        piece: 'http://search.paipai.com/cgi-bin/isuggest_paipai'
    });

    Manager.on('main.init',function( data ){
        this.set('word',data.keyword)
            .set('shipid',data.flagshipshopid)
            .set('title',data.itemdisplaystyle)
            .set('ban',data.isdirtyword)
            .set('landComm',data.showlpitem)
            .set('land',data.lpsearch)
            .set('navid',data.firstrootnavid )
            .set('landData',{
                title: data.lpitemtitle,
                id: data.lpitemid,
                brandid: data.lpitembrandid,
                classid: data.lpitemclassid,
                navid: data.lpitemnavid,
                degree: data.lpitemdegree,
                url: data.lpredirecturi,
                link: data.lpitemlink,
                sku: data.lpitemsku,
                sale: data.lpitemsellnum,
                price: data.lpitemprice,
                activity: data.lpitemactivityprice,
                img: data.lpitemvecimg
            })

    });

}( Alpha );;/**
 * Created by weiyanhai on 14/12/27.
 */
;!function( Alpha,Manager ){
    'use strict';
    var global = Function('return this')(),
        $ = Zepto;

    Manager.on('main.init',function( data ){
        var query,
            scene,
            screen,
            vk,
            grey,
            cookie,
            size_table;

        cookie = (function(){
            var cookie = document.cookie,
                hold,
                ret = {},
                length;

            cookie = cookie.split(';');
            length = cookie.length;

            while( length-- ){
                hold = cookie[ length].split('=');
                ret[ hold[ 0].replace(/^\s+/g,'') ] =  hold[ 1]  || undefined;
            }

            return ret;
        })();
        vk = (function(){
            var ck = /visitkey=(\d+)/gi.exec( document.cookie );

            if( ck ){
                ck = ck[ 1 ];

                return ck;
            }

            return null;
        })();
        grey = (function(){
            var v = vk,
                len,
                val;

            if( v === null ){
                return false;
            }

            len = v.length;
            val = v[ len - 7 ];

            return val % 2 == 0 ?
                true : false;

        })();
        query = parse();
        scene = (function( query ){

            if( query.searchshop == '1' ){
                return 'global_shop';
            }

            if( query.globalsearch == '1' ){
                return 'global_comm';
            }

            return 'shop_comm';
        })( query );
        screen = {
            width: global.screen.width,
            height: global.screen.height
        };
        size_table = {
            '160': 'img160',
            '200': 'imgL',
            '300': 'imgLL'
        }
        this.Info = Alpha.inhert('Info',query );

        Alpha.extend( this.Info = Alpha.inhert('Info',query ),{
            query: query,
            grey: true,
            vk: vk,
            cookie: cookie,
            login: cookie.wg_skey  && cookie.wg_uin || cookie.skey && cookie.uin ? true : false,
            get: function( k ){
                return this[k];
            },
            set: function( k,v ){
                this[ k ] = v;
                return this;
            },
            scene: scene,
            screen: screen,
            size: function( screen ){
                var width = Math.floor( screen.width / 2 );

                if( width > 200 ){
                    return size_table[ '300' ];
                }

                if( width > 160 ) {
                    return size_table[ '200' ];
                }

                return size_table[ '160' ];
            }( screen ),
            agent: function(){
                var ua = global.navigator.userAgent;

                return /micromessenger/g.test( ua ) && 'wechat' ||
                    /qq\/([\d\.]+)*/.test(ua) && 'qq' ||
                    '';
            }()
        });
    });

    function parse( href ){
        var ret = {},
            len,
            hold;

        href = href ||  global.location.search.slice(1);

        if( !href ){
            return ret;
        }

        href = href.split('&');
        len = href.length;

        while(len--){
            hold = href[len].split('=');
            ret[ hold[0] ] = hold[1];
        }

        return ret;
    }
}( Alpha,Manager );;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha,Manager ){
    var global = Function('return this')(),
        doc = document,
        extend = Alpha.extend,
        threshold = Manager.get('config.threshold');

    Manager.on('main.init',function(){
        var ctrl,
            info = this.Info,
            scene = info.get('scene'),
            word = this.get('word'),
            categoryid = this.Info.get('categoryid'),
            threshold = Manager.get('config.threshold');

        if( scene == 'shop_comm' || scene == 'global_comm' ){
            if( this.get('land') == '1' ){
                this.get('landComm') == '1' ?
                    info.set('scene','landing_page') : info.set('scene','cart')

            }
        }

        //注入属性

        $('[filter="true"]').attr({
            'event-point':'filter:move',
            'report-point': 'filter:btn'
        });

        $('.mod_bar2_input input').val( word );

        scene = info.get('scene');

        ctrl = Alpha.Ctrl.factory({
            elem: global,
            scan: true
        });

        if( categoryid ){
            Manager.set('categoryid',categoryid);

            info.set('keyword','');

            //this.get('title') == '1' ?
            //    ctrl.find('search_item_wrap').addClass('no_title') :
            //    '';
        }

        if( info.get('keyword') ){
            this.get('title') == '1' ?
                (ctrl.find('search_item_wrap').addClass('no_title'),this.set('no_title',true)) :
                '';
        }

        //设置列表，大图样式
        info.get('displaymode') == '2' ?
            ctrl.find('comm_item').addClass('matrix') :
            ctrl.find('comm_item').removeClass('matrix');

        info.get('displaymode') == '2' ?
            ctrl.find('view_type').addClass('matrix') :
            ctrl.find('view_type').removeClass('matrix');

        //设置关键字
        if( !categoryid ){
            scene == 'shop_comm' ?
                ctrl.find('search_input_shop_comm').val(word)
                : ctrl.find('search_input_global').val(word);

            this.get('title') == '1' ?
                (ctrl.find('search_item_wrap').addClass('no_title'),this.set('no_title',true)) :
                '';
        }

        //设置默认高度
        //ctrl.find('search_wrap').css('min-height',this.Info.get('screen').height + 'px');

        //监听事件
        ctrl.watch({
            'scroll': Alpha.throttle(function( e ){
                this.emit('scroll',e );
            },this.get('config.throttle'),ctrl),
            'resize': Alpha.throttle(function(e){
                Manager.emit('filter.all');
            },this.get('config.throttle'),ctrl),
            'tap [elem-point="btn_category"]': function(e){
                Manager.emit('cate.show');
            },
            'tap [event-point="cate:cancel"]': function(e){
                Manager.emit('cate.hide');
            },
            'tap [event-point="filter:move"]': function(e){
                switch( scene ){
                    case 'global_comm':
                    case 'cart':
                    case 'landing_page':
                        Manager.emit('filter.comm.show')
                        break;
                    case 'global_shop':
                        Manager.emit('filter.shop.show');
                        break;
                    case 'shop_comm':
                        Manager.emit('cate.show');

                }
            },
            'tap [event-point="back"],mod_bar_back': function(e){
                history.go(-1);
            },
            'tap [event-point="view"]': function( e ){
                var tar = $(e.currentTarget),
                    hasCls = tar.hasClass('matrix');

                hasCls ?
                    (ctrl.find('comm_item').removeClass('matrix'),tar.removeClass('matrix'))
                    : (ctrl.find('comm_item').addClass('matrix'),tar.addClass('matrix'));
            },
            'tap [event-point="aside:return"]': function(){
                global.setTimeout(function(){
                    $( global).scrollTop(0);
                },350)
            }
        });

        if( info.get('agent') == 'wx' || info.get('agent') == 'qq' ){
            ctrl.find('comm_head').addClass('mod_bar2_mobq')
            ctrl.find('global_head').addClass('mod_bar2_mobq')
        }
        else{
            ctrl.find('comm_head').removeClass('mod_bar2_mobq');
            ctrl.find('global_head').removeClass('mod_bar2_mobq');
        }

        if( scene == 'shop_comm' ){
            ctrl.find('shop_input').html(  )
        }
        else{

        }


        //扩展方法
        ctrl.next = function(){
            if( this.get('locked') ){
                return;
            }
            if( this.page ){
                this.page.next();
            }

            return this;
        }

        ctrl.set('locked',false);

        //注册事件
        ctrl.on('scroll',function( e ){
            var top,
                wHeight,
                dHeight,
                _global,
                _doc;

            if( this.get('locked') ){
                return;
            }

            _global = $( global );
            _doc = $( doc );

            top = _global.scrollTop();
            wHeight = _global.height();
            dHeight = _doc.height();

            if( (top + wHeight) > ( dHeight -  threshold) ){
                this.next();
            }
        });

        ctrl.on('scroll',function( e ){
            var top,
                height,
                _global;

            _global = $( global );
            top = _global.scrollTop();
            height = _global.height();

            top > height / 2 ?
                Manager.emit('aside.active.add') :
                Manager.emit('aside.active.remove');
        });

        //开放api
        Manager.on('main.show',function(){
            ctrl.find('loading_wrap').show();
            ctrl.find('main_wrap').show();
        });

        Manager.on('page.build',function(){
            var page,
                scene;

            scene = this.Info.get('scene');

            if( ctrl.page ){
                ctrl._old = ctrl.page;
            }

            page = Manager.page.build( scene );

            switch( scene ){
                case 'cart':
                case 'global_shop':
                case 'landing_page':
                    Manager.report.param.ssId = 31;
                    break;
                case 'global_comm':
                    Manager.report.param.ssId = 30;
                    break;
                case 'shop_comm':
                    Manager.report.param.ssId = 32;
                    break;
            }

            if( page=== false ){
                return;
            }

            ctrl.page = page;
            ctrl.next();
        });

        Manager.on('page.reset',function(){

            if( ctrl.page ){
                ctrl.page.reset();
                ctrl.page = null;
            }

            if( ctrl._old ){
                ctrl._old.reset();
                ctrl._old = null;
                Manager.Info.set('scene','shop_comm').set('oldScene',undefined);
                Manager.report.param.ssId = 32;
            }

            Manager.emit('supply.empty')
                .emit('delete.empty');

            this.emit('page.build');

            Manager.get('filter.state') == 'change' || Manager.get('filter.attr') ?
                ctrl.find('ship_wrap').hide() : ctrl.find('ship_wrap').show();
        });

        Manager.on('main.build',function(){
            switch( scene ){
                case 'shop_comm':
                    Manager.emit('page.build');
                    break;
                case 'global_shop':
                    Manager.emit('filter.shop.request');
                    break;
                case 'global_comm':
                    Manager.emit('filter.comm.request');
                    break;
                case 'cart':
                case 'landing_page':
                    Manager.emit('filter.comm.show',true);
                    break;
            }
        });

        (function(){
            var body = doc.body || doc.getElementsByTagName('body')[0],
                img = new Image();

            img.setAttribute('src','http://static.paipaiimg.com/fd/wd/h5/base/img/1x1.gif');
            img.setAttribute('style','height:1px;width:1px;display:none');

            body.appendChild(img);
            body.removeChild(img);
        })();

        Manager.set('page',{
            data: scene == 'cart' || scene == 'landing_page' ? (function(){
                var land = Manager.get('landData'),
                    navid = land.navid,
                    degree = land.degree,
                    brandid = land.brandid,
                    classid = land.classid,
                    ret = {};

                if( navid ){
                    ret.sClassid = navid;
                }

                if( degree ){
                    ret.degree = degree;
                }

                if( classid ){
                    ret.MultiCat = classid;
                }

                if( brandid ){
                    if( !(brandid == '66' || brandid == '329') ) {
                        navid ?
                            ret.Path = '0,' + navid + '-' + '55,' + brandid :
                            ret.Path = '55,' + brandid;
                    }
                    else{
                        ret.Path = '0,' + navid;
                    }
                }

                return ret;
            })(): {}
        });

        Manager.on( 'filter.all',function(){
            var _filter_attr = ctrl.find('filter_attr'),
                _cell_wrap ;

            if( !_filter_attr ){
                return;
            }

            _cell_wrap = _filter_attr.find('.cell_wrap');

            _cell_wrap.each(function( i,el ){
                var el = $( el),
                    par = $( el).parent(),
                    unit = par.parents('.row_unit'),
                    nxt = par.next('.btn_filter_more');

                if( el.height() < 69 ) {
                    unit.addClass( 'expand' );
                    nxt.hide();
                }
                else{
                    unit.removeClass( 'expand' );
                    nxt.show();
                }
            })
        });

        if( this.Info.get('scene') == 'cart' ){
            Manager.set('page.base',extend({},Manager.get('page.data')));
        }

        this.ctrl = ctrl;
    });



}( Alpha,Manager);
;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha,Manager ){
    var global = Function('return this')(),
        doc = document,
        threshold = Manager.get('config.threshold');

    Manager.on('main.init',function(){
        var ctrl = this.ctrl,
            _aside_wrap = ctrl.find('aside_wrap'),
            aside = Alpha.Ctrl.factory({
                elem: global,
                scan: true,
                event: {
                    'tap [event-point="aside:more"]': function(e,tar){
                        var par = tar.parents('.mod_bar2');

                        par.hasClass( 'mod_bar2_menu_unfold' ) ?
                            par.removeClass( 'mod_bar2_menu_unfold' ) : par.addClass( 'mod_bar2_menu_unfold' )
                    }
                }
            });

            Manager.on('aside.active.add',function(){
                _aside_wrap.addClass('mod_aside_top_active');
            });
            Manager.on('aside.active.remove',function(){
                _aside_wrap.removeClass('mod_aside_top_active');
            });
            Manager.emit('main.show');
    });

}( Alpha,Manager);
;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha,Manager ){
    var global = Function('return this')();

    Manager.on('main.init',function( data ){
        var scene = this.Info.get('scene'),
            extend = Alpha.extend,
            baseModel,
            wholeModel = {},
            exist = false;

        baseModel = (function(){
            var model = {},
                keyword = this.get('word'),
                scene = this.Info.get('scene');

            model.share_url = global.location.href;

            switch( scene ){
                case 'global_comm':
                    model.title = keyword + ' 精选商品 尽在拍拍';
                    model.desc = '好货哪里找，拍拍搜一把！潮流尖货，走过不要错过。';
                    break;
                case 'global_shop':
                    model.title = keyword + ' 精选微店 尽在拍拍';
                    model.desc = '微店哪家强，拍拍搜索来帮忙，只有想不到，没有搜不到。';
                    break;
                case 'shop_comm':
                    model.title = keyword + ' 被我承包了';
                    model.desc = '找 ' + keyword + ' 就上拍拍 '+ $('title').html();
            }

            return model;
        }).call( this );


        Manager.on('share.update',function( key ){
            var ptag = ['12475.18.1','12475.18.2','12475.18.3','12475.18.4','12475.18.5'],
                name = ['qq','qzone','weixin','weixinFriend','tqq'],
                len = name.length,
                w = wholeModel,
                hold;

            if( !exist ){
                baseModel.image_url = (scene == 'global_comm' || scene == 'shop_comm' ?
                    $('img[data-original]').eq(0).attr('data-original') :
                    $('.shop_cover img').eq(0).attr('src') ) || 'http://static.paipaiimg.com/v5/img/init_img.png';

                while( len-- ){
                    extend( hold = w[ name[ len ] ] = {},baseModel );
                    hold.ptag = ptag[ len ];
                }

                try{
                    paipai_h5_share.init( w );
                    exist = true;
                    Manager.off('share.update');
                }catch(e){
                    exist = false;

                }
            }
        });

        Manager.on('share.update.url',function( url ){
            var w = wholeModel,
                key;

            for( key in w ){
                w[ key].share_url = url;
            }

            try{
                paipai_h5_share.init( w );
            }catch(e){}

        });
    });

}( Alpha,Manager );;/**
 * Created by yandi_000 on 2015/3/24.
 */
;!function( Alpha,Manager ){
    var global = Function('return this')(),
        extend = Alpha.extend;

    Manager.on('main.init',function(){
        var ctrl = this.ctrl,
            body = document.body ||  document.getElementsByTagName('body')[0],
            AView,
            ACtrl;

        if( !1 ){
            return;
        }
        ACtrl = Alpha.Ctrl.factory({
            elem: body,
            event: {
                'tap [event-point="alert_cancel"]': function( e ){
                    var tar = $(e.currentTarget );
                    tar.parents('[remove-point]').remove();
                }
            }
        });

        AView = Alpha.View.factory({
            elem: document.body || document.getElementsByTagName('body')[0],
            template: ctrl.find('alert_tpl')
        });

        AView.render('format',function( model ){
            this.renderModel = model;
        });

        Manager.Alert = function( title,message,t ){
            AView.render({
                title: title,
                text: message
            });

            if( t ){
                setTimeout(function(){
                    $('[event-point="alert_cancel"]').trigger('tap');
                },t)
            }
        }
    });
}( Alpha,Manager );;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha,Manager ){
    var global = Function('return this')(),
        extend = Alpha.extend;

    Manager.on('main.init',function( data ){
        var scene = this.Info.get('scene'),
            screen = this.Info.get('screen'),
            word = encodeURIComponent( this.get('word')),
            item_serial,
            param,
            tap,
            pv,
            piece,
            ship,
            ptag = this.Info.get('ptag'),
            handle;

        scene == 'global_shop' ?
            item_serial = ['pvid','ssId','clickMsg1','pos','curPage','hitItemnum','defrownum'] :
            item_serial = ['pvid','ssId','classid','itemid','pos','curPage','hitItemnum','defrownum','extramsg'];

        param = {
            asFlag: 1,
            defItemnum: 10,
            screenHeight: screen.height,
            screenWidth: screen.width
        };

        scene == 'global_shop' ?
            ( param.defrownum = 1, param.ssId = 31) : ( param.defrownum = 2 , scene == 'global_comm' ) ?
                param.ssId = 30 : param.ssId = 32;

        piece = Alpha.PVReport.factory({
            param: param,
            ptag: ptag
        });

        pv = Alpha.PVReport.factory({
            param: param,
            ptag: ptag
        });

        pv.empty = scene == 'global_shop' ?
            function( key,sce ){
                var report;
                switch( key ) {
                    case 'site':
                        pv.site('&page=1&isearchshop=1');
                        break;
                    case 'search':
                        report = {
                            curPage: 1,
                            hitItemnum: 0,
                            keyword: word
                        }

                        if( sce ){
                            report = extend( report,sce )
                        }
                        pv.search( report );
                        break;
                }
                return this;
            } :
            function( key,sce ){
                var report,
                    attr = Manager.get('attr');
                switch( key ){
                    case 'site':
                        pv.site('&page=1',sce);
                        break;
                    case 'expo':
                        pv.expo({
                            hitItemnum: 0,
                            pos:'',
                            classid: '',
                            itemid: ''
                        });
                        break;
                    case 'search':
                        report = {
                            curPage: 1,
                            sortType: pv.parse('sort', Manager.get('sortType') ),
                            hitItemnum: 0,
                            keyword: word,
                            attrList: attr ? attr.join('-') : ''
                        }

                        if( sce ){
                            report = extend( report,sce )
                        }
                        pv.search( report );
                }
                return this;
            };

        tap = Alpha.TapReport.factory({
            elem: document,
            param: param,
            ptag: ptag,
            event: {
                'tap [report-point]': handle = function(e){
                    var tar = $( e.currentTarget ),
                        value = tar.attr('report-point'),
                        met,
                        args;

                    if( !value ){
                        return;
                    }

                    met = value.split(':');
                    args =  ([value,tar,e]).concat( met );

                    switch( met[0] ){
                        case 'item':
                            switch( met[1] ){
                                //case 'ship':
                                //    args.unshift('ship');
                                //    break;
                                default :
                                    args.unshift( 'item' );
                                    break;
                            }
                            break;
                        case 'view':
                            args.unshift('view');
                            break;
                        case 'piece':
                            args.unshift('piece');
                            break;
                        case 'filter':
                            switch( met[1] ){
                                case 'cate':
                                    args.unshift('filter.cate')
                                    break;
                                case 'addr':
                                    args.unshift('filter.addr')
                                    break;
                                default:
                                    args.unshift('function')
                            }
                            break;
                        case 'categoryid':
                            args.unshift('category');
                            break;
                        default :
                            args.unshift('function');
                    }

                    this.emit.apply( this,args );
                }
            }
        });

        tap.on('filter.addr',function(v,tar,e){
            var loc = this.tapTable[v],
                data = tar.attr('report-data'),
                report = {};

            data = this.parse('tap',data,['clickKeyword','clickPos']).param;
            loc = this.parse('locid',loc);

            data.clickKeyword = encodeURIComponent( data.clickKeyword );

            extend(report, data, loc);

            this.tapReport(report, tar);
        });

        tap.on('piece',function(v,tar,e){
            var loc = this.tapTable[v],
                data = tar.attr('report-data'),
                report = {};

            data = this.parse('tap',data,['pos','locmsg1']).param;
            loc = this.parse('locid',loc);

            data.locmsg1 = encodeURIComponent( data.locmsg1 );

            extend(report, data, loc);

            this.tapReport(report, tar);
        });

        tap.on('category',function(v,tar,e){
            var loc = this.tapTable[v],
                data = tar.attr('report-data'),
                report = {};

            data = this.parse('tap',data,['navid','navName']).param;
            loc = this.parse('locid',loc);

            data.navName = encodeURIComponent( data.navName );

            extend(report, data, loc);

            this.tapReport(report, tar);
        });

        tap.on('filter.cate',function(v,tar,e){
            var loc = this.tapTable[v],
                data = tar.attr('report-data'),
                report = {};

            data = this.parse('tap',data,['navid','navName','pos']).param;
            loc = this.parse('locid',loc);

            data.navid = data.navid.replace('-',',');
            data.navName = encodeURIComponent( data.navName );

            extend(report, data, loc);

            this.tapReport(report, tar);
        });

        tap.on('view',function( v,tar,e ){
            var hasCls = tar.hasClass('matrix'),
                loc = this.tapTable[ v],
                report = {};

            hasCls ?
                extend( report,{
                    showtype: 1
                },this.parse('locid',loc)) : extend( report,{
                showtype: 0
            },this.parse('locid',loc))

            this.tapReport(report, tar);
        });

        tap.on('ship', function (v, tar, e) {
            var loc = this.tapTable[v],
                data = tar.attr('report-data'),
                pvid,
                report = {};

            data = this.parse('tap',data,['pvid','ssId','clickMsg1','pos','curPage','hitItemnum','defrownum']).param;
            pvid = data.pvid;

            delete data.pvid;

            loc = this.parse('locid',loc);

            extend(report, data, loc);

            this.tapReport(report, tar, pvid);
        });

        tap.on('item', function (v, tar, e) {
            var loc = this.tapTable[v],
                data = tar.attr('report-data'),
                pvid,
                report = {};

            data = this.parse('tap',data,item_serial).param;
            pvid = data.pvid;

            delete data.pvid;

            loc = this.parse('locid',loc);

            extend(report, data, loc);

            this.tapReport(report, tar, pvid);
        });

        tap.on('function', function (v, tar, e) {
            var loc = this.tapTable[v],
                report = {};

            loc = this.parse('locid',loc);
            extend( report,loc );

            this.tapReport(report, tar);
        });

        Manager.report = {
            pv: pv,
            tap: tap,
            piece: piece,
            param: param
        }
    });

}( Alpha,Manager );;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha,Manager ){
    var global = Function('return this')(),
        extend = Alpha.extend;

    Manager.on('main.init',function( data ){
        var report = Manager.report.tap,
            scene = Manager.Info.get('scene');

        switch( scene ){
            case 'cart':
            case 'landing_page':
            case 'global_comm':
                report.tap( replace({
                    'filter:btn': '{{locid1}}-01-5'
                },'01'))

                report.tap( replace({
                    //back
                    'back': '{{locid1}}-01-3',

                    //tab
                    'tab:shop': '{{locid1}}-01-',
                    'tab:comm': '{{locid1}}-01-'
                },'01'))

                report.tap(replace({
                    // comm filter promo
                    'filter:promo:expinsive': '{{locid1}}-06-1',
                    'filter:promo:cash': '{{locid1}}-06-5',
                    'filter:promo:second': '{{locid1}}-06-6',
                    'filter:promo:fee': '{{locid1}}-06-8',
                    'filter:promo:ems': '{{locid1}}-06-2',

                    'filter:addr': '{{locid1}}-8-2',
                    'filter:address:btn': '{{locid1}}-8-1',

                    //comm filter category
                    'filter:cate': '{{locid1}}-07-'
                },'01'))

                report.tap( replace({
                    //global comm filter
                    'sort:default': '{{locid1}}-01-',
                    'sort:sales': '{{locid1}}-02-',
                    'sort:popular': '{{locid1}}-03-',
                    'sort:price': '{{locid1}}-04-'
                },'02'))

                report.tap(replace({
                    'view': '02-05-',
                    'piece': '03-06-',
                    'ship': '03-05-'
                }));

                report.tap( replace({
                    //global aside
                    'aside:more': '{{locid1}}-04-0',
                    'aside:return': '{{locid1}}-03-',
                    'aside:inexpinsive': '{{locid1}}-04-1',
                    'aside:home': '{{locid1}}-04-2',
                    'aside:user': '{{locid1}}-04-3',
                    'aside:order': '{{locid1}}-04-4',
                    'aside:fav': '{{locid1}}-05-5',

                    //global comm item
                    'item:comm:shop': '{{locid1}}-01-',
                    'item:comm:global': '{{locid1}}-02-',
                    'item:shop': '{{locid1}}-01-',
                    'item:ship': '{{locid1}}-05-'
                },'03'));
                break;
            case 'shop_comm':
                report.tap( replace({
                    'filter:btn': '{{locid1}}-01-6'
                },'11'))

                report.tap( replace({
                    'categoryid': '{{locid1}}-06-'
                },'11'));

                report.tap( replace({
                    //global comm filter
                    'sort:default': '{{locid1}}-01-',
                    'sort:sales': '{{locid1}}-02-',
                    'sort:popular': '{{locid1}}-03-',
                    'sort:price': '{{locid1}}-04-'
                },'12'))

                report.tap( replace({
                    //global aside
                    'aside:more': '{{locid1}}-04-0',
                    'aside:return': '{{locid1}}-03-',
                    'aside:inexpinsive': '{{locid1}}-04-1',
                    'aside:home': '{{locid1}}-04-2',
                    'aside:user': '{{locid1}}-04-3',
                    'aside:order': '{{locid1}}-04-4',
                    'aside:fav': '{{locid1}}-05-5',

                    //global shop item
                    'item:comm:shop': '{{locid1}}-01-',
                    'item:comm:global': '{{locid1}}-01-',
                    'item:ship': '{{locid1}}-05-'
                },'13'));

                break;
            case 'global_shop':
                report.tap( replace({
                    'filter:btn': '{{locid1}}-01-7'
                },'01'))

                report.tap( replace({
                    //back
                    'back': '{{locid1}}-01-3',

                    //tab
                    'tab:shop': '{{locid1}}-01-',
                    'tab:comm': '{{locid1}}-01-'
                },'11'))

                report.tap(replace({
                    // shop filter promo
                    'filter:promo:full': '{{locid1}}-09-1',
                    'filter:addr': '{{locid1}}-10-2',
                    'filter:address:btn': '{{locid1}}-10-1'

                    //comm filter category
                },'01'))

                report.tap( replace({
                    //global aside
                    'aside:more': '{{locid1}}-04-0',
                    'aside:return': '{{locid1}}-03-',
                    'aside:inexpinsive': '{{locid1}}-04-1',
                    'aside:home': '{{locid1}}-04-2',
                    'aside:user': '{{locid1}}-04-3',
                    'aside:order': '{{locid1}}-04-4',
                    'aside:fav': '{{locid1}}-05-5',

                    //global shop item
                    'item:shop': '{{locid1}}-01-'
                },'03'));

                report.tap( replace({
                    //global comm filter
                    'sort:default': '{{locid1}}-06-',
                    'sort:sales': '{{locid1}}-07-',
                    'sort:belief': '{{locid1}}-08-'
                },'02'))
        }

      

        function replace( object,locid ){
            var hasOwn = {}.hasOwnProperty;

            for( var key in object ){
                if( hasOwn.call( object,key ) ){
                    object[ key ] = object[ key ].replace('{{locid1}}',locid);
                }
            }
            return object;
        }
    });

}( Alpha,Manager );;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha,Manager ){
    var global = Function('return this')(),
        doc = document,
        _object_ = {},
        _hasOwn_ = _object_.hasOwnProperty,
        threshold = Manager.get('config.threshold'),
        sort_key_value = {
            'default': 77,
            'sales': 24,
            'popular': 21,
            'price:asc': 6,
            'price:desc': 7
        };

    Manager.on('main.init',function(){
        var ctrl = this.ctrl,
            info = this.Info,
            type = info.get('sorttype') || sort_key_value['default'],
            scene = this.Info.get('scene'),
            name,
            sort,
            elem;

        if(!(scene == 'global_comm' || scene == 'shop_comm' || scene == 'cart')){
            return;
        }

        name = (function( type,item ){
            var hasOwn = _hasOwn_;

            for(var key in item){
                if( hasOwn.call( item,key ) ){
                    if( type === item[key] ){
                        return key;
                    }
                }
            }

        }).call( this,type - 0,sort_key_value ) || 'default';

        this.set('sortType',type);
        ctrl.find('sort_wrap').find('.cur').removeClass('cur');

        sort = Alpha.Ctrl.factory({
            elem: ctrl.find('sort_wrap'),
            event: {
                'tap [event-point="sort"]': function( e,tar ){
                    this.emit('sort',e,tar);
                }
            }
        });

        elem = ctrl.find('sort_wrap').find('[sort-type="'+ name.replace(/\:(\w+)+/g,'') +'"]').addClass('cur');

        switch( name ){
            case 'price':
                name += ':asc';
            case 'price:asc':
                elem.addClass('asc');
                break;
            case 'price:desc':
                name += ':desc';
                elem.addClass('desc');
        }

        sort.set('locked',false)
            .set('type',sort_key_value[ name ]);

        sort.on('sort',function( e,tar ){
            var hasCls = tar.hasClass('cur'),
                type = tar.attr('sort-type') || '',
                isPrice = type == 'price',
                par = tar.parent(),
                asc;

            if( ctrl.get('locked') ){
                return;
            }

            if( hasCls && !isPrice ){
                return;
            }

            par.children().removeClass('cur');

            switch( type ){
                case 'default':
                case 'sales':
                case 'popular':
                    tar.addClass('cur');
                    break;
                case 'price':
                    asc = tar.attr('sort-asc');

                    if( asc == 'true' ){
                        type += ':desc';
                        asc = 'false';
                        tar.removeClass('asc').addClass('desc');
                    }
                    else{
                        type += ':asc';
                        asc = 'true';
                        tar.removeClass('desc').addClass('asc');
                    }

                    tar.addClass('cur');
                    tar.attr('sort-asc',asc);
                    break;
                default:
                    return;
            }

            this.set('type',sort_key_value[type]);

            Manager.set('sortType',sort_key_value[type]);
            Manager.emit('page.reset');
        });

        this.sort = sort;
    });

}( Alpha,Manager);
;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha,Manager ){
    var global = Function('return this')(),
        doc = document,
        _object_ = {},
        _hasOwn_ = _object_.hasOwnProperty,
        threshold = Manager.get('config.threshold'),
        sort_key_value = {
            'default': 10,
            'sales': 8,
            'belief': 4
        };

    Manager.on('main.init',function(){
        var ctrl = this.ctrl,
            info = this.Info,
            name,
            type = info.get('sorttype') || sort_key_value['default'],
            scene = this.Info.get('scene'),
            sort,
            elem;

        if(!(scene == 'global_shop')){
            return;
        }

        name = (function( type,item ){
            var hasOwn = _hasOwn_;

            for(var key in item){
                if( hasOwn.call( item,key ) ){
                    if( type === item[key] ){
                        return key;
                    }
                }
            }

        }).call( this,type - 0,sort_key_value ) || 'default';

        this.set('sortType',sort_key_value[ name ]);
        ctrl.find('sort_wrap').find('.cur').removeClass('cur');

        sort = Alpha.Ctrl.factory({
            elem: ctrl.find('sort_wrap'),
            event: {
                'tap [event-point="sort"]': function( e,tar ){
                    this.emit('sort',e,tar);
                }
            }
        });

        elem = ctrl.find('sort_wrap').find('[sort-type="'+ name.replace(/\:(\w+)+/g,'') +'"]').addClass('cur');

        switch( name ){
            case 'price':
                name += ':asc';
            case 'price:asc':
                elem.addClass('asc');
                break;
            case 'price:desc':
                name += ':desc';
                elem.addClass('desc');
        }

        sort.set('locked',false)
            .set('type',sort_key_value[ name ]);

        sort.on('sort',function( e,tar ){
            var hasCls = tar.hasClass('cur'),
                type = tar.attr('sort-type') || '',
                isPrice = type == 'price',
                par = tar.parent(),
                asc;

            if( this.get('locked') ){
                return;
            }

            if( hasCls && !isPrice ){
                return;
            }

            par.children().removeClass('cur');

            switch( type ){
                case 'default':
                case 'sales':
                case 'belief':
                    tar.addClass('cur');
                    break;
                case 'price':
                    asc = tar.attr('sort-asc');

                    if( asc == 'true' ){
                        type += ':desc';
                        asc = 'false';
                        tar.removeClass('asc').addClass('desc');
                    }
                    else{
                        type += ':asc';
                        asc = 'true';
                        tar.removeClass('desc').addClass('asc');
                    }

                    tar.addClass('cur');
                    tar.attr('sort-asc',asc);
                    break;
                default:
                    return;
            }

            this.set('type',sort_key_value[type]);

            Manager.set('sortType',sort_key_value[type]);
            Manager.emit('page.reset');
        });

        this.sort = sort;
    });

}( Alpha,Manager);
;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha,Manager ){
    var global = Function('return this')(),
        extend = Alpha.extend,
        bigInt = global.bigInt;

    Manager.on('main.init',function( data ){
        var ctrl = this.ctrl,
            scene = Manager.Info.get('scene'),
            screen = Manager.Info.get('screen'),
            keyword = Manager.Info.get('keyword'),
            times = Manager.get('config.times'),
            index = 0,

            _main_wrap = ctrl.find('main_wrap'),
            _filter_comm_wrap = ctrl.find('filter_comm_wrap').show(), //修改 名称
            _category_filter_wrap = ctrl.find('category_fliter_wrap'),
            _filter_cate_tpl = ctrl.find('filter_cate_tpl'),
            _filter_select = ctrl.find('filter_select'),
            _filter_attr = ctrl.find('filter_attr'),
            _filter_hd = ctrl.find('filter_hd'),
            _filter_clear = ctrl.find('filter_clear'),

            param,
            fCtrl,
            fView,
            AView,
            fPipe;

        if( !(scene == 'global_comm' || scene == 'cart' || scene == 'landing_page') ){
            return;
        }

        Manager.set('filter',{
            state: 'default',
            attr: false
        });

        Manager.set('attr',[]);

        _filter_comm_wrap.css('min-height', screen.height + 'px');

        fPipe = Alpha.Pipe.factory();

        fView = Alpha.View.factory({
            elem: _category_filter_wrap,
            template: _filter_cate_tpl,
            url: Manager.get('api.comm'),
            dataType: 'jsonp'
        });

        AView = Alpha.View.factory({
            elem: _filter_attr,
            template: _filter_cate_tpl ,
            dataType: 'jsonp',
            url: Manager.get('api.comm')
        });

        AView.request('before',function( param ){
            var attr = Manager.get('attr_param');

            extend( param,{
                dataType: 'jsonp',
                timeout: 5000,
                data: extend({
                    PageSize: 10,
                    PageNum: 1,
                    charset: 'gbk',
                    dtype: 'jsonp',
                    g_tk: 12,
                    KeyWord: keyword,
                    OrderStyle: Manager.get('sortType') || 77,
                    ac: 1,
                    sf: 2007,
                    cluster: 1
                }, attr ? {Path: attr}: {})
            });
        });

        AView.request('done',function( ret ){
            var data = ret.data,
                attr = data.Property.slice(1);

            try{

                if( ret.retCode == '0' ){
                    if( attr.length > 0 ){
                        this.render( attr,function( el,html,model ){
                            el.html( html );
                        });
                    }
                    else{
                        this.emit('request.fail',ret)
                    }
                }
                else{
                    this.emit('request.fail',ret)
                }

            }catch(e){
                this.emit('request.fail',e);
            }
        });

        AView.request('fail',function(){
            _filter_hd.addClass('no_attr');
        });

        AView.render('after',function(){
            _filter_hd.removeClass('no_attr');
        });

        AView.template = '[[#attr]]<div class="row_unit attr_unit">' + AView.template + '</div>[[/attr]]';

        fView.request('before',function(param ){
            extend( param,{
                dataType: 'jsonp',
                timeout: 5000,
                data: extend({
                    PageSize: 10,
                    PageNum: 1,
                    charset: 'gbk',
                    dtype: 'jsonp',
                    g_tk: 12,
                    KeyWord: keyword,
                    OrderStyle: Manager.get('sortType')|| 77,
                    ac: 1,
                    sf: 2007,
                    cluster: 1
                })
            });

        });

        fView.request('done',function( ret ){
            var data = ret.data,
                property = data.Property[0],
                name;

            try{
                name = property[0];

                if( ret.retCode == '0' ){
                    if( property ){
                        this.render( ret,function( el,html,model ){
                            if( name == 'cluster' ){
                                el.html( html );
                            }
                        });
                    }
                    else{
                        this.emit('request.fail',ret)
                    }
                }
                else{
                    this.emit('request.fail',ret)
                }

            }catch(e){
                this.emit('request.fail',e);
            }

        });

        AView.render('format',function( item ){
            var model = {},
                length = item.length;

            while( length-- ){
                item[ length ].name = item[ length ][ 0 ];
                item[ length ].item = item[ length ][ 1 ];
                item[ length].btnName = item[ length ][ 0 ];
            }

            model.attr = item;
            model.prop = 'path';

            this.renderModel = model;
        });



        fView.render('before',function( ret ){
            AView.data = ret.data.Property.slice( 1 );

            if( !Manager.Info.get('classpath') ){
                Manager.set('commItem', ret);
            }

            if( !(AView.data.length > 0) ){
                _filter_hd.addClass( 'no_attr' );
                Manager.set('no_attr',true);
            }
            else{
                _filter_hd.removeClass( 'no_attr' );
                AView.render();

                Manager.set('attr_data',AView.data );
                Manager.set('no_attr',false);
                AView.data = undefined;
            }
        });

        fView.request('fail',function( err,message ){
            index < times ?
                ( Manager.emit('filter.comm.request'),index++) :
                Manager.emit('page.reset');
        });

        fView.render('format',function( ret ){
            var model = {},
                hold = ret.data.Property[0][1],
                idx = 0;

            hold = hold.slice(0,10);
            model.prop = 'path';
            model.name = '商品类目';
            model.btnName = '类目';
            model.item = hold;
            model.handleReport = function(){
                return function(){
                    return 'report-point="filter:cate"';
                }
            }
            model.handlePosition = function(){
                return function(){
                    return ++idx;
                }
            }

            this.renderModel = model;
        });

        fCtrl = Alpha.Filter.factory({
            elem: _filter_comm_wrap,
            event: {
                'tap [event-point="filter_confirm"],[event-point="filter:confirm"]': function(){
                    _main_wrap.removeClass('move');

                    if( !( Manager.get('state') == 'supply' ) ){

                        fPipe.inject();
                    }
                },
                'tap .promo_unit [filter-point]': function(e){
                    var tar = $(e.currentTarget),
                        name = tar.attr('filter-point'),
                        hasCls = tar.hasClass('selected'),
                        value,
                        type;

                    if( name ){
                        name = name.split(':');
                        type = name[0];
                        value = name[1];


                        hasCls ?
                            (tar.removeClass('selected'),this.remove( type,value ) )
                            : (tar.addClass('selected'),this.insert( type,value ));
                    }
                },
                'tap [filter-point]': function( e ){
                    setTimeout(function(){
                        var child = _filter_comm_wrap.find( '.selected' ),
                            exist = child.length > 0;

                        exist ?
                            _filter_clear.show() : _filter_clear.hide();
                    },0);
                },
                'tap .attr_unit [filter-point]': function( e ){
                    var tar = $(e.currentTarget),
                        name = tar.attr('filter-point'),
                        hasCls = tar.hasClass('selected'),
                        par = tar.parents('.row_unit'),
                        hold,
                        len,
                        temp,
                        value,
                        type;

                    if( name ){
                        name = name.split(':');
                        type = name[0];
                        value = name[1];

                        if( hasCls ){
                            tar.removeClass('selected');
                            this.remove( type,value );

                            len = attr.length;

                            while( len-- ){
                                if( attr[ len ] == value ){
                                    attr.splice( len,1 );
                                    break;
                                }
                            }

                        }
                        else{
                            hold = par.find('.selected');
                            if( hold.length > 0 ){
                                temp = hold.attr('filter-point').split(':');
                                this.remove( temp[0],temp[1] );
                            }
                            hold.removeClass('selected');
                            tar.addClass('selected');
                            this.insert( type,value );
                        }
                    }
                },
                'tap [event-point="filter:clear"]': function( e ){
                    _filter_comm_wrap.find( '.selected').removeClass( 'selected' );
                    _filter_clear.hide();

                    fCtrl.set('path',[])
                        .set('property',[])
                        .set('degree', undefined)
                        .set('address',undefined)
                        .set('payType',undefined)

                    Manager.set('attr_param',undefined)
                    Manager.emit('recovery');
                },
                'tap [event-point="filter:attr"]': function( e ){
                    var tar = $( e.currentTarget),
                        prev = tar.prev();

                    if( tar.hasClass('cur') ){
                        return;
                    }

                    prev.removeClass( 'cur' );
                    tar.addClass( 'cur' );

                    _filter_attr.show();
                    _filter_select.hide();

                    Manager.emit( 'filter.all');
                },
                'tap [event-point="filter:select"]': function( e ){
                    var tar = $( e.currentTarget),
                        next = tar.next();

                    if( tar.hasClass('cur') ){
                        return;
                    }

                    next.removeClass( 'cur' );
                    tar.addClass( 'cur' );

                    _filter_attr.hide();
                    _filter_select.show();
                },
                'tap [event-point="filter:more"]': function(e){
                    var tar = $(e.currentTarget),
                        par = tar.parents('.row_unit');

                    par.hasClass('expand') && !par.hasClass('addr_unit') ?
                        par.removeClass('expand') : par.addClass('expand');
                },
                'tap .addr_unit [filter-point],.cate_unit [filter-point]': function(e){
                    var tar = $(e.currentTarget),
                        name = tar.attr('filter-point'),
                        hasCls = tar.hasClass('selected'),
                        par = tar.parents('.row_unit'),
                        value,
                        temp,
                        hold,
                        type;

                    if( name ){
                        name = name.split(':');
                        type = name[0];
                        value = name[1];

                        this.set('path',[]);

                        if( hasCls ){
                            tar.removeClass('selected');
                            this.remove( type,value );

                            if( par.hasClass( 'cate_unit' ) ){
                                Manager.get('attr_param',undefined );
                                Manager.emit('recovery');
                            }
                        }
                        else{
                            par.find('.selected').removeClass('selected');
                            tar.addClass('selected');

                            this.insert( type,value );

                            if( !Manager.get('no_attr') ){
                                if( par.hasClass( 'cate_unit' ) ){
                                    Manager.set('attr_param',value );
                                    AView.request();
                                }
                            }
                        }

                    }
                }
            }
        });

        fCtrl.set('property',[]);
        fCtrl.set('path',[]);

        fCtrl.calculate('property',function(){
            var item = this.get('property'),
                len = item.length,
                sum;

            if( !len ){
                return undefined;
            }

            while( len-- ){
                sum = sum ?
                    sum.or( item[len] ) : bigInt( item[len]);
            }

            return sum.toString();
        });

        fCtrl.calculate('path',function(){
            var item = this.get('path').concat([]),
                len = item.length,
                sum;

            if( !len ){
                return undefined;
            }

            return item.sort().join('-');
        });

        fPipe.on('done',function(){
            var p = param || Manager.get('page.data'),
                base = Manager.get('page.base'),
                address = fCtrl.get('address'),
                degree = fCtrl.get('degree'),
                payType = fCtrl.get('payType'),
                property = fCtrl.calculate('property'),
                path = fCtrl.calculate('path');

            if( base ){
                address === undefined ?
                    base.Address ? p.Address = base.Address : delete p.Address
                    : p.Address = address;
                property === undefined ?
                    base.Property ? p.Property = base.Property : delete p.Property
                    : p.Property = property;
                degree === undefined ?
                    base.degree ? p.degree = base.degree : delete p.degree
                    : p.degree = degree;
                path === undefined ?
                    base.Path ? p.Path = base.Path : delete p.Path
                    : p.Path = path;
                payType === undefined ?
                    base.Paytype ? p.Paytype = base.Paytype : delete p.Paytype
                    : p.Paytype = payType;
            }
            else{
                path === undefined ?
                    delete p.Path : p.Path = path;
                address === undefined ?
                    delete p.Address : p.Address = address;
                degree === undefined ?
                    delete p.degree : p.degree = degree;
                payType === undefined ?
                    delete p.Paytype : p.Paytype = payType;
                property === undefined ?
                    delete p.Property : p.Property = property;
            }

        });

        fPipe.on('done',function(){
            var p = param || Manager.get('page.data'),
                base = Manager.get('page.base');

            if( base ){
                if( p.Address === base.Address &&
                    p.Path === base.Path &&
                    p.degree === base.degree &&
                    p.Paytype === base.Paytype &&
                    p.Property === base.Property
                ){
                    Manager.set('filter.state','default');
                    Manager.set('filter.attr', false );
                }
                else{
                    _filter_attr.find('.selected').length > 0 ?
                        Manager.set('filter.attr', true ) :
                        Manager.set('filter.attr', false )

                    _filter_select.find('.selected').length > 0 ?
                        Manager.set('filter.state', 'change') :
                        Manager.set('filter.state', 'default')
                }
            }
            else{
                if( p.Address === undefined &&
                    p.Path === undefined &&
                    p.degree === undefined &&
                    p.Paytype === undefined &&
                    p.Property === undefined
                ){
                    Manager.set('filter.state', 'default');
                    Manager.set('filter.attr', false );
                }
                else{
                    _filter_attr.find('.selected').length > 0 ?
                        Manager.set('filter.attr', true ) :
                        Manager.set('filter.attr', false )

                    _filter_select.find('.selected').length > 0 ?
                        Manager.set('filter.state', 'change') :
                        Manager.set('filter.state', 'default')
                }
            }

            ctrl.find('filter_item').html('');
            Manager.emit('page.reset');
        })

        fPipe.inject(function( operate ){
            var p = param || (param = Manager.get('page.data')),
                base = Manager.get('page.base'),
                ctrl = fCtrl,
                path = ctrl.calculate('path'),
                address = ctrl.get('address'),
                degree = ctrl.get('degree'),
                payType = ctrl.get('payType'),
                property = ctrl.calculate('property');

            if( base ){
                path = path || base.Path;
                address = address || base.Address;
                degree = degree || base.degree;
                payType = payType || base.Paytype;
                property = property || base.Property;

                if( path == p.Path &&
                    address == p.Address &&
                    degree == p.degree &&
                    payType == p.Paytype &&
                    property == p.Property
                ){
                    return operate.fail();
                }

                operate.next();
            }
            else{
                if( path == p.Path &&
                    address == p.Address &&
                    degree == p.degree &&
                    payType == p.Paytype &&
                    property == p.Property
                ){
                    return operate.fail();
                }
                operate.next();
            }
        });

        fCtrl.on('cart.select',function(){
            var wrap = _filter_comm_wrap,
                item = [];

            item.push(
                ['degree',Manager.get('landData.degree')]
            );

            Alpha.each(item,function(i,elem){
                var select = '[filter-point="'+ elem[0] +':'+ elem[1] +'"]';

                select = wrap.find(select);

                if( select.length > 0 ){
                    select.addClass('selected');
                    fCtrl.insert( elem[0],elem[1] );
                }
            });

            setTimeout(function(){
                var child = _filter_comm_wrap.find( '.selected' ),
                    exist = child.length > 0;

                exist ?
                    _filter_clear.show() : _filter_clear.hide();
            },0);

            fPipe.emit('done');
        });

        fCtrl.on('request.select',function(){
            var info = Manager.Info,
                wrap = _filter_comm_wrap,
                item = [],
                path,
                property = info.get('property');

            item.push(
                ['address', info.get('address')],
                ['degree',info.get('degree')],
                ['payType',info.get('paytype')]
            );

            Alpha.each(item,function(i,elem){
                var select = '[filter-point="'+ elem[0] +':'+ elem[1] +'"]';

                select = wrap.find(select);

                if( select.length > 0 ){
                    select.addClass('selected');
                    fCtrl.insert( elem[0],elem[1] );
                }
            });

            Alpha.each( ( path = info.get('classpath') ) ? path.split('-') : [],function(i,elem){
                var select = '[filter-point="path:'+ elem +'"]';

                select = wrap.find(select);

                if( select.length > 0 ){
                    select.addClass('selected');
                    fCtrl.insert( 'path',elem );
                }
            });

            setTimeout(function(){
                var child = _filter_comm_wrap.find( '.selected' ),
                    exist = child.length > 0;

                exist ?
                    _filter_clear.show() : _filter_clear.hide();
            },0);

            if( !(property === undefined) ){
                property = bigInt( property );

                wrap.find('[filter-point*="property"]').each(function(i,elem){
                    var tar = $( elem),
                        attr = tar.attr('filter-point'),
                    value = attr.split(':')[1];

                    if( !(property.and( value).toString() == '0') ){
                        tar.addClass('selected');
                        fCtrl.insert( 'property',value );
                    }
                });
            }

            fPipe.emit('done');
        });

        fView.render('after',function(){
            fCtrl.emit('request.select');
            fCtrl.set('exist',true);
            Manager.emit('filter.delay.show');
        });

        Manager.on('recovery',function(){
            if( !Manager.get('no_attr') ){
                AView.data = Manager.get('attr_data');

                AView.render(function( el,html ){
                    el.html( html );
                });

                AView.data = undefined;
            }
        });

        Manager.on('filter.comm.request',function(){
            fView.request();
        });
        
        Manager.on('filter.comm.show',function( force ){
            !fCtrl.get('exist') && !force ?
                Manager.once('filter.delay.show',function(){
                    _main_wrap.addClass('move')
                }) : false;

             force ?
                fCtrl.emit('cart.select') :
                _main_wrap.addClass('move')

        });

        Manager.on('filter.comm.hide',function(){
            _main_wrap.removeClass('move');
        })
    });

}( Alpha,Manager );;/**
 * Created by yan.Div on 2014/12/27.
 */
;!function( Alpha,Manager ){
    var global = Function('return this')(),
        _object_ = {},
        _hasOwn_ = _object_.hasOwnProperty,
        extend = Alpha.extend;

    Manager.on('main.init',function(){
        var cateView,
            shopid = Manager.Info.get('shopid'),
            ctrl = this.ctrl,
            timer = null,

            _cate_wrap = ctrl.find('cate_wrap'),
            _search_bd_wrap = ctrl.find('search_bd_wrap'),
            _result_wrap = ctrl.find('result_wrap'),

            CCtrl;

        if( !(this.Info.get('scene') == 'shop_comm') ){
            return;
        }

        //ctrl.find('result_wrap').css('min-height',Manager.Info.get('screen').height - 173 - 49 + 'px');
        _result_wrap.css('min-height',Manager.Info.get('screen').height - 179 - 49 + 'px');

        Manager.set('filter',{
            state: 'default'
        });

        cateView = Alpha.View.factory({
            elem: _cate_wrap,
            url: this.get('api.cate'),
            template: ctrl.find('cate_tpl'),
            dataType: 'jsonp'
        });

        CCtrl = Alpha.Ctrl.factory({
            elem: _cate_wrap,
            event: {
                'tap .cate_item a': function(e){
                    var tar = $(e.currentTarget),
                        child = tar.children('span'),
                        name = tar.attr('filter-point'),
                        hasCls = child.hasClass('selected'),
                        par = tar.parents('.cate_bd_wrap'),
                        value,
                        type;

                    if( name ){
                        name = name.split(':');
                        type = name[0];
                        value = name[1];

                        if( type == 'categoryid' ){
                            if( hasCls ){
                                child.removeClass('selected');
                                Manager.set('filter.state','default');
                                value = undefined;
                            }
                            else{
                                par.find('.selected').removeClass('selected');
                                child.addClass('selected');
                                Manager.set('filter.state','change');
                            }

                            clearTimeout( timer );

                            timer = global.setTimeout(function(){
                                Manager.set('categoryid',value);
                                Manager.emit('cate.hide');
                                Manager.emit('page.reset');
                            },350)
                        }
                    }
                }

            }
        });

        cateView.request('before',function( param ){
            extend(param,{
                data:{
                    shopid: shopid,
                    datatype: 'jsonp'
                },
                dataType: 'jsonp'
            })
        });

        cateView.request('done',function( ret ){
            var cate = ret.data.category;

            if( ret.error == '0' ){
                this.render( ret );
            }
        });

        cateView.render('format',function( ret ){
            var data = ret.data,
                model;

            extend( this.renderModel = model = {},{
                item: data.category
            });

            handle( data.category );

            function handle( item ){
                var len = item.length,
                    id = [],
                    i = 0,
                    hold,
                    cate;

                while( i < len ){
                    hold = item[ i ];
                    hold._id = hold.id;
                    cate = [];
                    id.push( hold.id );
                    if( hold.subCategory ){
                        if( hold.subCategory.length > 0 ){
                            cate = handle( hold.subCategory );
                        }
                    }
                    cate.unshift( hold.id );
                    hold.categoryid = cate.join(',');
                    i++;
                }
                return id;
            }

            model.item = data.category;
            model.allCategoryid = 0;
            model.allName = '全部类别';

        });

        cateView.render('after',function(ret){
            var categoryid = Manager.get('categoryid');

            this.set('exist',true);

            if( categoryid ){
                cateView._elem.find('[filter-point*="categoryid:'+ categoryid +'"] span').addClass('selected');
            }

            _search_bd_wrap.addClass('move');
            _result_wrap.css('height',_cate_wrap.height() + 179 + 'px');

        });

        Manager.on('cate.show',function(){
            var view = cateView;

            if( view.get('exist') ){
                _search_bd_wrap.addClass('move');
                _result_wrap.css('height',_cate_wrap.height() + 179 + 'px');
            }
            else{
                if( view.hold ){
                    view.hold.abort();
                }
                view.request();
            }
        });

        Manager.on('cate.hide',function(){
            _search_bd_wrap.removeClass('move');
            _result_wrap.css('height','auto');
        });

    });

}( Alpha,Manager );
;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha,Manager ){
    var global = Function('return this')(),
        extend = Alpha.extend;

    Manager.on('main.init',function( data ){
        var ctrl = this.ctrl,
            scene = Manager.Info.get('scene'),
            screen = Manager.Info.get('screen'),

            _main_wrap = ctrl.find('main_wrap'),
            _filter_shop_wrap = ctrl.find('filter_shop_wrap').show(), //修改 名称

            param,
            fCtrl,
            fPipe;

        if( !(scene == 'global_shop') ){
            return;
        }

        Manager.set('filter',{
            state: 'default'
        });

        _filter_shop_wrap.css('min-height',screen.height + 'px');

        fPipe = Alpha.Pipe.factory();

        fCtrl = Alpha.Filter.factory({
            elem: _filter_shop_wrap,
            event: {
                'tap [event-point="filter:confirm"]': function(){
                    _main_wrap.removeClass('move');

                    fPipe.inject();
                },
                'tap .promo_unit [filter-point]': function(e){
                    var tar = $(e.currentTarget),
                        name = tar.attr('filter-point'),
                        hasCls = tar.hasClass('selected'),
                        value,
                        type;

                    if( name ){
                        name = name.split(':');
                        type = name[0];
                        value = name[1];

                        if( hasCls ){
                            tar.removeClass('selected');
                            this.remove( type,value );
                        }
                        else{
                            tar.addClass('selected');
                            this.insert( type,value );
                        }
                    }
                },
                'tap [event-point="filter:more"],.btn_filter_more': function(e){
                    var tar = $(e.currentTarget),
                        par = tar.parents('.row_unit');

                    par.addClass('expand');
                },
                'tap .addr_unit [filter-point],.cate_unit [filter-point]': function(e){
                    var tar = $(e.currentTarget),
                        name = tar.attr('filter-point'),
                        hasCls = tar.hasClass('selected'),
                        par = tar.parents('.row_unit'),
                        value,
                        type;

                    if( name ){
                        name = name.split(':');
                        type = name[0];
                        value = name[1];

                        if( hasCls ){
                            tar.removeClass('selected');
                            this.remove( type,value );
                        }
                        else{
                            par.find('.selected').removeClass('selected');
                            tar.addClass('selected');
                            this.insert( type,value );
                        }
                    }
                }
            }
        });

        fCtrl.set('property',[]);

        fCtrl.calculate('property',function(){
            var item = this.get('property'),
                len = item.length,
                sum = 0;

            if( !len ){
                return undefined;
            }

            while( len-- ){
                sum = sum ?
                    sum.or( item[len] ) : bigInt( item[len]);
            }

            return sum.toString();
        });

        fPipe.on('done',function(){
            var p = param || Manager.get('page.data'),
                ctrl = fCtrl,
                address = ctrl.get('address'),
                property = ctrl.calculate('property');

            address === undefined ?
                delete p.Address : p.Address = address;
            property === undefined ?
                delete p.Property : p.Property = property;

            Manager.emit('page.reset');
        },-1);


        fPipe.on('done',function(){
            var p = param || Manager.get('page.data');

            if( p.Address === undefined &&
                p.Property === undefined
            ){
                Manager.set('filter',{
                    state: 'default'
                });
            }
            else{
                Manager.set('filter',{
                    state: 'change'
                });
            }
        });

        fPipe.inject(function( operate ){
            var p = param || (param = Manager.get('page.data')),
                ctrl = fCtrl;

            if( ctrl.get('address') == p.Address &&
                ctrl.calculate('property') == p.Property
            ){
                return operate.fail();
            }
            operate.next();
        });

        fCtrl.on('select',function(){
            var info = Manager.Info,
                wrap = _filter_shop_wrap,
                property = info.get('property'),
                item = [];

            item.push(
                ['address', info.get('address')]
            );

            Alpha.each(item,function(i,elem){
                var select = '[filter-point="'+ elem[0] +':'+ elem[1] +'"]';

                select = wrap.find(select);

                if( select.length > 0 ){
                    select.addClass('selected');
                    fCtrl.insert( elem[0],elem[1] );
                }
            });

            if( !(property === undefined) ){
                property = bigInt( property );

                wrap.find('[filter-point*="property"]').each(function(i,elem){
                    var tar = $( elem),
                        attr = tar.attr('filter-point');
                    value = attr.split(':')[1];

                    if( !(property.and( value).toString() == '0') ){
                        tar.addClass('selected');
                        fCtrl.insert( 'property',value );
                    }
                });
            }

            fPipe.emit('done');
        });

        Manager.on('filter.shop.request',function(){
            fCtrl.emit('select');
        });
        
        Manager.on('filter.shop.show',function(){
            _main_wrap.addClass('move');
        });

        Manager.on('filter.shop.hide',function(){
            _main_wrap.removeClass('move');
        })
    });

}( Alpha,Manager );;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha,Manager ){
    var global = Function('return this')(),
        extend = Alpha.extend;

    Manager.on('main.init',function( data ){
        var shipView,
            ctrl = this.ctrl,
            shipid = this.get('shipid'),
            scene = this.Info.get('scene'),
            pv = Manager.report.pv,
            en_word = encodeURIComponent( Manager.get('word') );

        if( !(scene == 'global_comm') ){
            return;
        }

        if( !shipid ){
            return;
        }

        if( Manager.get('filter.state') == 'change' ){
            return;
        }

        shipView = Alpha.View.factory({
            elem: ctrl.find('ship_wrap'),
            template: ctrl.find('page_shop_tpl'),
            url: this.get('api.shop'),
            dataType: 'jsonp'
        });

        shipView.request('done',function( ret ){
            var item = ret.data.shopList;

            ret.retCode == '0' ?
                item.length > 0 ? this.render( ret ) : this.emit('fail'):
                this.emit('fail',ret);
        });

        shipView.request('before',function( p ){
            extend(p,{
                timeout:5000,
                data: {
                    KeyWord: shipid,
                    PageType: 4,
                    PageNum: 1,
                    PageSize: 10,
                    sf: 1002
                }
            });
        });

        shipView.render('before',function( ret ){
            var s = ret.data.shopList[0];

            pv.report('expo',extend({
                    hitItemnum: 1,
                    defrownum: 1,
                    defItemnum:1,
                    itemid: s.shopUin,
                    extraMsg: 'fss',
                    pos: 1
                }));
        });

        shipView.render('format',function( ret ){
            var model = {},
                hold = ret.data.shopList[0];

            if( parseInt( hold.shopComScore ) > 0) {
                hold.shopUrl += '?ptag=20511.2.2';
                model.item = [hold];

                model.pvid = pv.pvid();

                model.total = 1;
                model.point = 'ship';
                model.source = 30;
                model.row = 1;
                model.handlePosition = function(){
                    return function(txt,render){
                        return 1;
                    }
                }

                model.handleShip = function(){
                    return function(text,render){
                        return render('<span class="icon_shipshop"></span>');
                    }
                }

                model.handleName = function(){
                    return function( txt,render ){
                        return render( this['shopName'] );
                    }
                }
                model.handleLoc = function(){
                    return function( txt,render ){
                        return this['shopAddress'] == '未选择' ? '' : '<p class="shop_loc">所在地：'+ render( this['shopAddress'] ) +'</p>';
                    }
                }
                model.handleCate = function(){
                    return function( txt,render ){
                        return this['mainCategory'] == '未填写' ? '' : '<p class="shop_cate">所有类目：'+ render( this['mainCategory'] ) +'</p>';
                    }
                }

                this.renderModel = model;
            }
        });

        shipView.request();

    });

}( Alpha,Manager );;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha,Manager ){
    var global = Function('return this')(),
        extend = Alpha.extend;

    Manager.on('main.init',function( data ){
        var ctrl = this.ctrl,
            landCtrl,
            landView,
            landData = Manager.get('landData'),
            itemid = landData.id,
            _landing_page = ctrl.find('landing_page');


        if( !(Manager.Info.get( 'scene' ) == 'landing_page') ){
            return;
        }

        //http://m.paipai.com/tws/shopcart/addcmdy?daid=126&commlist=' +

        landView = Alpha.View.factory({
            elem: _landing_page,
            template: ctrl.find('ld_tpl'),
            dataType: 'jsonp',
            data: landData.img
        });

        landCtrl = Alpha.Ctrl.factory({
            elem: ctrl.find('landing_page'),
            event: {
               /* 'tap [event-point="in_cart"]': function( e ){
                    var tar = $(e.currentTarget),
                        url = 'http://ui.ptlogin2.paipai.com/cgi-bin/login?style=9&pt_no_onekey=1&appid=100273020&hln_css=http://static.paipaiimg.com/fd/paipai/base/img/logo_h5.png&low_login=0&pt_ttype=1&ssid=9FEBFB986A4BDCAAF8E7B69F63AFB3E1&s_url=';

                    if( !Manager.Info.login ){
                        return location.href = url + encodeURIComponent( location.href );
                    }

                    $.ajax({
                        url: 'http://m.paipai.com/tws/shopcart/addcmdy',
                        dataType: 'jsonp',
                        success: function( ret ){
                            ret.retCode == '0' ?
                                Manager.Alert( '温馨提示','商品已加入购物车' ,1500) :
                                Manager.Alert( '温馨提示','商品加入购物车失败',1500);
                        },
                        error: function(){
                            Manager.Alert( '温馨提示','商品加入购物车失败',1500);
                        },
                        data:{
                            daid:126,
                            commlist: itemid,
                            type: 1,
                            g_tk: '',
                            g_ty: 'ls'
                        }
                    })
                },*/
                'tap [event-point="in_fav"]': Alpha.throttle(function( e ){
                    var tar = $(e.currentTarget),
                        url = 'http://ui.ptlogin2.paipai.com/cgi-bin/login?style=9&pt_no_onekey=1&appid=100273020&hln_css=http://static.paipaiimg.com/fd/paipai/base/img/logo_h5.png&low_login=0&pt_ttype=1&ssid=9FEBFB986A4BDCAAF8E7B69F63AFB3E1&s_url=',
                        hasCls = _landing_page.find( '.fav_wrap_on').length > 0;

                   window.DelFavItemListCallBack = function( ret ){
                       ret.retCode == '0' ?
                           (Manager.Alert('温馨提示','取消成功',1500),  tar.removeClass('fav_wrap_on')) :
                           ret.retCode == '13' ?
                               location.href = url + encodeURIComponent( location.href ): Manager.Alert('温馨提示','取消失败',1500);

                       window.DelFavItemListCallBack = undefined;
                   }

                    hasCls ?
                        $.ajax({
                            url: 'http://favorite.my.paipai.com/favorite/DelFavItemList',
                            dataType: 'jsonp',
                            jsonpCallback: 'DelFavItemListCallBack',
                            success: function(){},
                            error: function(){
                                Manager.Alert('温馨提示','取消成功',1500);
                                _landing_page.find( '.fav_wrap').removeClass( 'fav_wrap_on' )
                            },
                            data: {
                                itemId: itemid,
                                g_tk: '',
                                g_ty: 'ls'
                            }
                        }) : $.ajax({
                            url: 'http://favorite.my.paipai.com/favorite/AddFavItemList',
                            dataType: 'jsonp',
                            success: function( ret ){
                                ret.retCode == '0' ?
                                    (Manager.Alert('温馨提示','成功关注',1500),  tar.addClass('fav_wrap_on')) :
                                    ret.retCode == '13' ?
                                        location.href = url + encodeURIComponent( location.href ): Manager.Alert('温馨提示','关注失败',1500);
                            },
                            error: function(){
                                Manager.Alert('温馨提示','关注失败',1500);
                            },
                            data: {
                                itemid: itemid,
                                g_tk: '',
                                g_ty: 'ls'
                            }
                        })

                },200)
            }
        });

        landView.render('format',function( ret ){
            var model = {},
                data = landData;

            model.item = function(){
                return function(){
                    var item = data.img || [],
                        html = '',
                        i= 0,
                        length = item.length;

                    while( i < length ){
                        html += '<div class="swiper-slide"><img src="'+ item[ i++ ] +'"></div>';
                    }
                    return html;
                }
            }
            model.text = '查看商品详情';
            model.href = data.link;
            model.fee = 12.00;
            model.comm_name = data.title;
            model.price = data.price;
            model.handleActiveStyle = function(){
                return function(){
                    return  data.activity ? 'style="text-decoration:line-through;color:#666;font-size:14px;"': '';
                }
            }
            model.handleActive = function(){
                return function(){
                    return data.activity ? '<div class="ld_active_price"><span>&yen;<em>'+ data.activity +'</em></span><a>活动价</a></div>' : '';
                }
            }
            model.sale = data.sale;

            this.renderModel = model;
        });

        landView.render('after',function( ret ){
            var swiper = new Swiper('.slider', {
                pagination: '.pagination',
                slidesPerView: 'auto',
                paginationClickable: true,
                centeredSlides: true,
                spaceBetween: 0,
                loop: true,
                loopAdditionalSlides: 1,
                loopedSlides: landData.img.length
            });

            $.ajax({
                url: 'http://favorite.my.paipai.com/favorite/CheckFavItemList',
                dataType: 'jsonp',
                data: {
                    comm_id_list: itemid,
                    g_tk: '',
                    g_ty: 'ls'
                },
                success: function( ret ){
                    var data;
                    if( ret.retCode == '0'){
                        data = ret.data;
                        if( data[ itemid ] == '1' ){
                            _landing_page.find( '.fav_wrap').addClass( 'fav_wrap_on' );
                        }
                    }
                },
                error: function(){

                }
            });

        });

        landView.render(function( elem,html ){
            elem.prepend( html );
        });
    });

}( Alpha,Manager );;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha,Manager ){
    var global = Function('return this')(),
        extend = Alpha.extend;

    Manager.on('main.init',function( data ){
        var page = this.page || ( this.page = {} ),
            categoryid = this.Info.get('categoryid');

        Manager.set('categoryid',categoryid);

        Manager.set('categoryid',Manager.Info.get('categoryid'));

        page.buildShopComm = function(){
            var manager = Manager,
                scene,
                size,
                word,
                keyword,
                en_word,
                shopid,
                shop,
                data,
                page,
                ctrl = manager.ctrl,
                pv = manager.report.pv,

                _shop_comm_item = ctrl.find('shop_comm_item'),
                _loading_wrap = ctrl.find('loading_wrap'),
                _shop_comm_emtpy = ctrl.find('shop_comm_empty'),
                _global_promo = ctrl.find('global_promo'),
                _load_done = ctrl.find('load_done'),
                _load_timeout = _load_done.clone().attr('id','load_timeout').css('display','block'),
                _shop_comm_item = ctrl.find('shop_comm_item'),
                _global_comm_empty = ctrl.find('global_comm_empty'),
                _filter_empty_comm = ctrl.find('filter_empty_comm'),

            keyword = manager.Info.get('keyword');
            word = manager.get('word');
            shopid = manager.Info.get('shopid');
            scene = manager.Info.get('scene');
            en_wrod = encodeURIComponent( word );

            _global_comm_empty.find('em').text( word );
            _shop_comm_emtpy.find('em').text( word );
            _global_promo.find('em').text( word );
            _filter_empty_comm.find('em').text( word );

            if( manager.get('ban') == '1' ){
                _shop_comm_item.append( _global_comm_empty.show() );
                _loading_wrap.hide();
                return false;
            }

            if( categoryid ){
                keyword = '';
            }

            page = Alpha.Page.factory({
                elem: _shop_comm_item,
                template: ctrl.find('page_comm_tpl'),
                url:manager.get('api.comm'),
                dataType: 'jsonp'
            });

            page.set('locked',false);

            page.request('before',function( p ){
                var shop = shopid,
                    cate = manager.get('categoryid');

                if( cate ){
                    shop += '.' + cate;
                }

                extend( p,{
                    dataType: 'jsonp',
                    timeout: 5000,
                    data: extend({},manager.get('page.data'),{
                        PageNum: this.current,
                        PageSize: size = 10,
                        charset: 'gbk',
                        dtype: 'jsonp',
                        g_tk: 12,
                        KeyWord: keyword,
                        OrderStyle: manager.get('sortType'),
                        ac: 1,
                        sf: 2007,
                        shop: shop
                    })
                });

                ctrl.set('locked',true);
                _loading_wrap.show();
                _load_timeout.hide();
            });

            page.request('done',function( ret ){
                var data = ret.data,
                    total = data.totalNum;

                ret.retCode == '0' ?
                    total > 0 ?
                        this.render( ret )
                            .emit('update',ret)
                        : this.emit('none',ret)
                    : this.emit('request.fail',ret);

            });

            page.on('update',function( ret ){
                var sum = this.current * size,
                    total = ret.data.totalNum;

                sum >= total ?
                    this.emit('end') : this.emit('finish');
            });

            page.request('fail',function( err ){
                var extra,
                    state,
                    method,
                    extraObject = {};

                if( state == 'change' ){
                    extra = extra || [];
                    extra.push( 'h5_st_3' );
                }

                if( extra ){
                    extraObject.extraMsg = extra.join('~');
                }

                if( manager.get('no_title') === true ){
                    extraObject.version = '101' + Manager.get('navid');
                }
                pv.empty('expo').empty('search', extraObject);

                this.current > 1 ?
                    method = 'append' : method = 'html';

                //this.set('locked',true);
                ctrl.set('locked',false);

                if( this.current > 1 ){
                    _load_timeout.find('p').html('加载失败，请重新加载...');
                }
                else{
                    _load_timeout.find('p').html('加载失败，请重新刷新...');
                }

                this.current -= 1;

                _loading_wrap.hide();
                _shop_comm_item[ method ]( _load_timeout.show() );
            });

            page.render('before',function( ret ){
                var data = ret.data,
                    extra,
                    extraObject = {};

                if( manager.get('filter.state') == 'change' ){
                    extra = extra || [];
                    extra.push( 'h5_st_3' );
                }

                if( extra ){
                    extraObject.extraMsg = extra.join('~');
                }

                if( manager.get('no_title') === true ){
                    extraObject.version = '101' + Manager.get('navid');
                }

                pv.report('site','page=' + this.current)
                    .report('search',{
                        curPage: this.current,
                        sortType: pv.parse('sort', Manager.get('sortType') ),
                        hitItemnum: data.totalNum,
                        keyword: en_wrod
                    },extraObject)
                    .report('expo',extend({
                        hitItemnum: data.totalNum
                    },pv.parse('expo',ret.data.list)));
            });

            page.render('after',function(){
                _shop_comm_item.find('img').lazyload();
                Manager.emit('share.update');
            });

            page.render('format',function( ret ){
                var model = {},
                    idx = 0,
                    data = ret.data,
                    grey = Manager.Info.get('grey'),
                    size = manager.Info.get('size');

                model.page = this.current;
                model.item = ret.data.list;
                model.pvid = pv.pvid();
                model.total = data.totalNum;
                model.point = 'item:comm:shop';
                model.source = 32;
                model.row = 2;
                model.handlePosition = function(){
                    return function( text,render ){
                        return ++idx;
                    }
                }

                model.handleSale = function(){
                    return function(){
                        var saleNum = this.saleNum - 0;

                        if( saleNum > 1000000 ){
                            saleNum = (Math.round( saleNum / 10000 )).toFixed( 0 ) + '万';
                        }
                        else if( saleNum >= 10000 ){
                            saleNum = ( saleNum / 10000 ).toFixed( 1 ) + '万';
                        }

                        return this.saleNum == '0' ? '' : '<p class="comm_sale_num">销量'+ saleNum +'件</p>';
                    }
                }

                model.handlePrice = function(){
                    return function(){
                        var price = ( this.price - 0 ).toFixed(0);

                        return ( price - 0 ) === ( this.price - 0 ) ?
                            price : this.price
                    }
                }

                model.handleState = function(){
                    return function(){
                        switch ( this.PersonalizedTypeTag ){
                            case '1':
                                return '<i class="icon_bought">已购买</i>';
                            case '2':
                                return '<i class="icon_browsed">已浏览</i> '
                        }
                    }
                }

                model.handleType = function(){
                    return function(){
                        switch( this.Active_Price_Type ){
                            case '1':
                                return 'ppy';
                            case '3':
                                return 'sg';
                        }
                    }
                }

                model.handleLink = function(){
                    return function(){
                        switch( this.Active_Price_Type ){
                            case '1':
                                return this.paiPianYiLink;
                            default:
                                return this.h5link;
                        }
                    }
                }

                model.handlePromo = function(){
                    return function( txt,render ){

                        switch( this.Active_Price_Type ){
                            case '1':
                                return '<i class="icon_expinsive"></i>';
                            case '3':
                                return '<i class="icon_quickbuy"></i>'
                        }
                    }
                }
                model.handleTitle = function(){
                    return function( txt,render ){
                        return render( this['title'] )
                    }
                }
                model.handleImage = function(){
                    return function( txt,render ){
                        return render( this[size] );
                    }
                }

                this.renderModel = model;
            });

            page.on('none',function(){
                var extra,
                    extraObject = {},
                    state = manager.get('filter.state');


                if(state == 'change' ){
                    extra = extra || [];
                    extra.push( 'h5_st_3' );
                }

                if( extra ){
                    extraObject.extraMsg = extra.join('~');
                }

                if( manager.get('no_title') === true ){
                    extraObject.version = '101' + Manager.get('navid');
                }

                this.set('locked',true);
                ctrl.set('locked',false);

                pv.empty('expo').empty('search',extraObject);

                manager.get('categoryid') ?
                    (_shop_comm_item.append( state == 'change' ? _filter_empty_comm.show(): _load_done.show() ), _loading_wrap.hide())
                    : (manager.once('tips.shop.comm',function(){
                        _global_promo.removeClass('expand');

                        _shop_comm_item.append( _shop_comm_emtpy.show() );
                        _shop_comm_item.append( _global_promo.show() );
                    }),manager.Info.set('oldScene',scene).set('scene','global_comm'),manager.emit('page.build'));

            });
            page.on('end',function(){
                this.set('locked',true);
                ctrl.set('locked',false);

                manager.get('categoryid') ?
                    (_shop_comm_item.append(_load_done.show()),_loading_wrap.hide() )
                    : (manager.once('tips.shop.comm',function(){
                        _global_promo.addClass('expand');

                        _shop_comm_item.append( _global_promo.show() );
                    }),manager.Info.set('oldScene',scene).set('scene','global_comm'),manager.emit('page.build'));
            });
            page.on('finish',function(){
                ctrl.find('loading_wrap').hide();
                ctrl.set('locked',false);
            });

            Manager.once('tips.shop.comm.empty',function(){
                page.empty();
            });

            return page;
        }

    });

}( Alpha,Manager );;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha,Manager ){
    var global = Function('return this')(),
        _object_ = {},
        _array_ = [],
        _slice_ = _array_.slice,
        _hasOwn_ = _object_.hasOwnProperty,
        extend = Alpha.extend;

    Manager.on('promo.build',function( data ){
        var ctrl = this.ctrl,
            PView,
            state = Manager.get('filter.state'),
            keyword = Manager.Info.get('keyword'),
            en_word = encodeURIComponent( Manager.get('word')),
            pv = Manager.report.pv;

        if( state == 'default' && !Manager.get('filter.attr') ){
            return;
        }


        PView = Alpha.View.factory({
            template: ctrl.find('delete_tpl'),
            elem: ctrl.find('filter_item'),
            url: Manager.get('api.comm'),
            dataType: 'jsonp'
        });

        PView.request('before',function( param ){
            extend( param,{
                dataType: 'jsonp',
                timeout: 5000,
                data: extend({},{
                    charset: 'gbk',
                    g_tk: 12,
                    KeyWord: keyword,
                    dtype: 'jsonp',
                    PageNum: 1,
                    PageSize: 4,
                    OrderStyle: 77,
                    PageType: 4,
                    sf: 1004
                })
            });

        })

        PView.request('fail',function(){
            pv.empty('site').empty('expo',{
                defItemnum: 4
            }).empty('search',
                {
                    extraMsg: 'h5_st_8',
                    defItemnum: 4
                }
            );
        })

        PView.request('done',function( ret ){
            var data = ret.data,
                item = data.list;


            if( ret.retCode == '0' ){
                if( item.length > 3 ){
                    this.render( ret );
                }
            }
        });

        PView.render('before',function( ret ){
            var data = ret.data;

            pv.report('site','page=1',true)
                .report('search',{
                    curPage: 1,
                    sortType: pv.parse('sort', Manager.get('sortType') ),
                    hitItemnum: data.totalNum,
                    keyword: en_word,
                    defItemnum: 4
                },{extraMsg: 'h5_st_8'})
                .report('expo',extend({
                    hitItemnum: data.totalNum,
                    defItemnum: 4
                },pv.parse('expo',ret.data.list.slice(0,4))));
        })

        PView.render('format',function( ret ){
            var model = {},
                that = this,
                idx = 0,
                data = ret.data,
                grey = Manager.Info.get('grey'),
                size = Manager.Info.get('size');

            model.handleCommTpl = function(){
                return function(){
                    return that.engine.render( ctrl.find('page_comm_tpl').html(),{
                        item: ret.data.list,
                        page: 1,
                        pvid: pv.pvid(),
                        total: data.totalNum,
                        point: 'item:comm:global',
                        source: 30,
                        row: 2,
                        handlePrice : function(){
                            return function(){
                                var price = ( this.price - 0 ).toFixed(0);

                                return ( price - 0 ) === ( this.price - 0 ) ?
                                    price : this.price
                            }
                        },
                        handleSale : function(){
                            return function(){
                                var saleNum = this.saleNum - 0;

                                if( saleNum > 1000000 ){
                                    saleNum = (Math.round( saleNum / 10000 )).toFixed( 0 ) + '万';
                                }
                                else if( saleNum >= 10000 ){
                                    saleNum = ( saleNum / 10000 ).toFixed( 1 ) + '万';
                                }

                                return this.saleNum == '0' ? '' : '<p class="comm_sale_num">销量'+ saleNum +'件</p>';
                            }
                        },
                        handlePosition: function(){
                            return function(){
                                return ++idx;
                            }
                        },
                        handleType: function(){
                            return function(){
                                switch( this.Active_Price_Type ){
                                    case '1':
                                        return 'ppy';
                                    case '3':
                                        return 'sg';
                                }
                            }
                        },
                        handleLink: function(){
                            return function(){
                                switch( this.Active_Price_Type ){
                                    case '1':
                                        return this.paiPianYiLink;
                                    default:
                                        return this.h5link;
                                }
                            }
                        },
                        handleState: function(){
                            return function(){
                                switch ( this.PersonalizedTypeTag ){
                                    case '1':
                                        return '<i class="icon_bought">已下单</i>';
                                    case '2':
                                        return '<i class="icon_browsed">已浏览</i> '
                                }
                            }
                        },
                        handleTitle: function(){
                            return function( txt,render ){
                                return render( this['title'] )
                            }
                        },
                        handlePromo : function(){
                            return function( txt,render ){
                                switch( this.Active_Price_Type ){
                                    case '1':
                                        return '<i class="icon_expinsive"></i>';
                                    case '3':
                                        return '<i class="icon_quickbuy"></i>';
                                }
                            }
                        },
                        handleImage : function(){
                            return function( txt,render ){
                                return render( this[size] );
                            }
                        }
                    })
                }
            }

            model.handleWord = function(){
                return function(){
                    return '我们为您找到了以下商品：';
                }
            }

            model.handleFootTpl = function(){
                return function(){
                    return that.engine.render( ctrl.find('delete_foot_tpl').html(),{
                        btnTotal: data.totalNum,
                        btnLink: (function(){
                            var location = global.location,
                                host = location.origin,
                                path = location.pathname,
                                query = Manager.Info.query,
                                hasOwn = _hasOwn_,
                                ret = [];

                            for( var k in query ){
                                if( hasOwn.call( query,k ) ){
                                    switch( k ){
                                        case 'address':
                                        case 'degree':
                                        case 'paytype':
                                        case 'saletype':
                                        case 'classpath':
                                        case 'pricerange':
                                        case 'sorttype':
                                        case 'property':
                                            continue;
                                            break;
                                        default :
                                            ret.push( k + '=' + query[ k ] );
                                    }
                                }
                            }

                            return host + path + '?' + ret.join('&');
                        })()
                    })
                }
            }

            this.renderModel = model;

        });

        PView.render('after',function(a,b,c){
            ctrl.find('filter_item').find('img').lazyload();
            Manager.emit('share.update');
        })

        PView.request();
    });

}( Alpha,Manager );;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha,Manager ){
    var global = Function('return this')(),
        extend = Alpha.extend;

    Manager.on('main.init',function( data ){
        var page = this.page || ( this.page = {} );

        page.buildGlobalComm = function(){
            var manager = Manager,
                size,
                word,
                en_word,
                keyword,
                data,
                page,
                threshold = manager.get('config.threshold'),
                ctrl = manager.ctrl,
                pv = manager.report.pv,
                piece = manager.get('config.piece'),
                offset = manager.Info.get('offset'),
                current,

                _global_comm_item = ctrl.find('global_comm_item'),
                _global_comm_empty = ctrl.find('global_comm_empty'),
                _loading_wrap = ctrl.find('loading_wrap'),
                _load_done = ctrl.find('load_done'),
                _load_timeout = _load_done.clone().attr('id','load_timeout').css('display','block'),
                _filter_empty_comm = ctrl.find('filter_empty_comm'),

            keyword = manager.Info.get('keyword') || '';
            word = manager.get('word');
            en_word = encodeURIComponent( word );

            _global_comm_empty.find('em').text(word);
            _filter_empty_comm.find('em').text(word);

            if( manager.get('ban') == '1' ){
                _global_comm_item.append( _global_comm_empty.show() );
                _loading_wrap.hide();
                return false;
            }

            size = 10;

            if( offset ){
                current = parseInt( offset / size );
            }

            current = current || 0;


            page = Alpha.Page.factory({
                elem: _global_comm_item,
                template: ctrl.find('page_comm_tpl'),
                url:manager.get('api.comm'),
                dataType: 'jsonp'
            });
            page.current = current;
            page.set('locked',false);

            if( !Manager.Info.get('classpath') ){
                var next = page.next;

                if( Manager.get('commItem') ){
                    page.next = function(){
                        if( this.get('locked') ){
                            return;
                        }

                        this.current++;

                        page.emit('request.done', Manager.get('commItem'));

                        page.next = next;
                        Manager.set('commItem',undefined);

                        return this;
                    }
                }
            } //

            page.request('before',function( p ){
                extend( p,{
                    dataType: 'jsonp',
                    timeout: 5000,
                    data: extend({},manager.get('page.data'),{
                        PageNum: this.current,
                        PageSize: size,
                        charset: 'gbk',
                        dtype: 'jsonp',
                        g_tk: 12,
                        KeyWord: keyword,
                        OrderStyle: manager.get('sortType') || 77,
                        ac: 1,
                        sf: 2007
                    })
                });

                ctrl.set('locked',true);
                _loading_wrap.show();
                _load_timeout.hide();
            });

            page.request('done',function( ret ){
                var data = ret.data,
                    total = data.totalNum - 0;

                ret.retCode == '0' ?
                    total < 1 ?
                        this.emit('none',ret)
                        : (this.render(ret).emit('update',ret), total > 1 && total < 9 ? this.emit('supply',ret) : false )

                    : this.emit('request.fail',ret);
            });

            page.on('update',function( ret ){
                var sum = this.current * size,
                    total = ret.data.totalNum;

                sum >= total ?
                    this.emit('end',ret ) : this.emit('finish');
            });

            page.request('fail',function( err,message ){
                var extra,
                    extraObject = {},
                    state = manager.get('filter.state'),
                    attr = manager.get('filter.attr'),
                    method;

                if( state == 'change' ){
                    extra = extra || [];
                    extra.push( 'h5_st_2' );
                }

                if( attr ){
                    extra = extra || [];
                    extra.push( 'h5_st_8' );
                }

                if( extra ){
                    extraObject.extraMsg = extra.join('~');
                }

                if( manager.get('no_title') === true ){
                    extraObject.version = '101' + Manager.get('navid');
                }

                pv.empty('expo').empty('search',extraObject);

                this.current > 1 ?
                    method = 'append' : method = 'html';

                ctrl.set('locked',false);

                if( this.current > 1 ){
                    _load_timeout.find('p').html('加载失败，请重新加载...');
                }
                else{
                    _load_timeout.find('p').html('加载失败，请重新刷新...');
                }

                this.current -= 1;

                _global_comm_item[ method ]( _load_timeout.show() )

                _loading_wrap.hide();

                //this.emit('supply');
            });

            page.render('before',function( ret ){
                var data = ret.data,
                    extra,
                    attr = Manager.get('attr'),
                    extraObject = {};

                if( manager.get('filter.state') == 'change' ){
                    extra = extra || [];
                    extra.push( 'h5_st_1' );
                }

                if( manager.get('filter.attr') ){
                    extra = extra || [];
                    extra.push( 'h5_st_7' );
                }

                if( extra ){
                    extraObject.extraMsg = extra.join('~');
                }

                if( manager.get('no_title') === true ){
                   extraObject.version = '101' + Manager.get('navid');
                }

                manager.emit('tips.shop.comm');

                pv.report('site','page=' + this.current,manager.Info.get('globalsearch') == '1' ? false : true)
                    .report('search',{
                        curPage: this.current,
                        sortType: pv.parse('sort', Manager.get('sortType') ),
                        hitItemnum: data.totalNum,
                        keyword: en_word,
                        attrList: attr ? attr.join('-') : ''
                    },extraObject)
                    .report('expo',extend({
                        hitItemnum: data.totalNum
                    },pv.parse('expo',ret.data.list)));
            });

            page.render('after',function(){
                var _img = _global_comm_item.find('img');

                _img.lazyload();

                if(this.current % piece == 0 ){
                    Manager.emit('piece.build',page._elem.find('.page_item').last(),keyword,word);
                }

                Manager.emit('share.update');
            });

            page.render('format',function( ret ){
                var model = {},
                    idx = 0,
                    data = ret.data,
                    size = manager.Info.get('size'),
                    grey = Manager.Info.get('grey');

                model.page = this.current;
                model.item = ret.data.list;
                model.pvid = pv.pvid();
                model.total = data.totalNum;
                model.point = 'item:comm:global';
                model.source = 30;
                model.row = 2;
                model.handlePosition = function(){
                    return function( text,render ){
                        return ++idx;
                    }
                }

                model.handleSale = function(){
                    return function(){
                        var saleNum = this.saleNum - 0;

                        if( saleNum > 1000000 ){
                            saleNum = (Math.round( saleNum / 10000 )).toFixed( 0 ) + '万';
                        }
                        else if( saleNum >= 10000 ){
                            saleNum = ( saleNum / 10000 ).toFixed( 1 ) + '万';
                        }

                        return this.saleNum == '0' ? '' : '<p class="comm_sale_num">销量'+ saleNum +'件</p>';
                    }
                }

                model.handlePrice = function(){
                    return function(){
                        var price = ( this.price - 0 ).toFixed(0);

                        return ( price - 0 ) === ( this.price - 0 ) ?
                            price : this.price
                    }
                }

                model.handleType = function(){
                    return function(){
                        switch( this.Active_Price_Type ){
                            case '1':
                                return 'ppy';
                            case '3':
                                return 'sg';
                        }
                    }
                }

                model.handleLink = function(){
                    return function(){
                        switch( this.Active_Price_Type ){
                            case '1':
                                return this.paiPianYiLink;
                            default:
                                return this.h5link;
                        }
                    }
                }

                model.handleState = function(){
                    return function(){
                        switch ( this.PersonalizedTypeTag ){
                            case '1':
                                return '<i class="icon_bought">已下单</i>';
                            case '2':
                                return '<i class="icon_browsed">已浏览</i> '
                        }
                    }
                }

                model.handlePromo = function(){
                    return function( txt,render ){
                        switch( this.Active_Price_Type ){
                            case '1':
                                return '<i class="icon_expinsive"></i>';
                            case '3':
                                return '<i class="icon_quickbuy"></i>';
                        }
                    }
                }
                model.handleTitle = function(){
                    return function( txt,render ){
                        return render( this['title'] )
                    }
                }
                model.handleImage = function(){
                    return function( txt,render ){
                        return render( this[size] );
                    }
                }

                this.renderModel = model;
            });

            page.on('none',function(){
                var extra,
                    extraObject = {},
                    state = manager.get('filter.state'),
                    attr = manager.get('filter.attr');

                if( state == 'change' ){
                    extra = extra || [];
                    extra.push( 'h5_st_1' );
                }

                if( attr ){
                    extra = extra || [];
                    extra.push( 'h5_st_7' );
                }

                if( extra ){
                    extraObject.extraMsg = extra.join('~');
                }

                if( manager.get('no_title') === true ){
                    extraObject.version = '101' + Manager.get('navid');
                }

                this.set('locked',true);
                ctrl.set('locked',false);

                keyword ?
                    _global_comm_item.html( state == 'change' || attr ? _filter_empty_comm.show() :_global_comm_empty.show() )
                    : _global_comm_item.html( state == 'change' || attr ? _filter_empty_comm.show() :_global_comm_empty.show() )


                pv.empty('site').empty('expo').empty('search',extraObject);

                _loading_wrap.hide();

                Manager.emit('tips.shop.comm.empty');

                if( state == 'default' && !Manager.get('filter.attr') ){
                    Manager.set('state','supply');
                }

                this.emit('supply');
            });
            page.on('end',function( ret ){

                this.set('locked',true);
                ctrl.set('locked',false);

                _global_comm_item.append( _load_done.show() );
                _loading_wrap.hide();
            });
            page.on('finish',function(){
                ctrl.set('locked',false);

                _loading_wrap.hide();
                Manager.emit('tips.shop.comm');
            });

            page.on('supply',function( ret ){
                Manager.emit('promo.build',ret);
            });

            Manager.on('page.none',function(){
                page.emit('fail');
            });

            return page;
        }

    });

}( Alpha,Manager );;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha,Manager ){
    var global = Function('return this')(),
        extend = Alpha.extend;

    Manager.on('promo.build',function( data ){
        var ctrl = this.ctrl,
            suppView,
            scene = Manager.Info.get('scene'),
            state = Manager.get('filter.state'),
            _supply_bd = ctrl.find('supply_bd'),
            keyword = this.get('word'),
            pv = Manager.report.pv,
            en_word = encodeURIComponent( Manager.get('word') );

        if( !(state == 'default') || Manager.get('filter.attr') ){
            return;
        }

        suppView = Alpha.View.factory({
            url: Manager.get('api.supp'),
            elem: _supply_bd,
            template: ctrl.find('page_comm_tpl'),
            dataType: 'jsonp'
        });

        suppView.request('before',function( p ){
            extend(p,{
                data: {
                    key: scene == 'cart' ? Manager.get('landData.title') : keyword,
                    g_ty: 'ls'
                }
            });
        });

        suppView.request('fail',function(){
            pv.empty('site').empty('expo',{
                defItemnum: 4
            }).empty('search',
                {
                    extraMsg: 'h5_st_6',
                    defItemnum: 4
                }
            );
        });

        suppView.request('done',function(ret){
            var product = ret.Product;

            try{
                if( product.length > 4){
                    ret.Product = Alpha.map( ret.Product.slice(0,4),function(i,elem){
                        var m = {};
                        elem.link = 'http://m.jd.com/product/' + elem.wareid + '.html';
                        elem.price = (elem.dredisprice - 0).toFixed(2);
                        elem.imgUrl = 'http://img13.360buyimg.com/n1/' + elem.Content.imageurl;
                        elem.title = elem.Content.warename;
                        elem.LeafClassId = elem.catid;
                        elem.commId = elem.wareid;

                        return elem;
                    });

                    return this.render( ret );
                }
                this.emit('request.fail');
            }
            catch(e){
                this.emit('request.fail');
            }
        });

        suppView.render('before',function( ret ){
            var total = ret.Summary.ResultCount;

            pv.report('site','page=1',true)
                .report('search',{
                    curPage: 1,
                    sortType: pv.parse('sort', Manager.get('sortType') ),
                    hitItemnum: total,
                    keyword: en_word,
                    defItemnum: 4
                },{extraMsg: 'h5_st_6'})
                .report('expo',extend({
                    hitItemnum: total,
                    defItemnum: 4
                },pv.parse('expo',ret.Product)));
        })

        suppView.render('format',function( ret ){
            var model = {},
                idx = 0;

            model.item = ret.Product;

            model.page = 1;
            model.pvid = pv.pvid();
            model.total = ret.Summary.ResultCount;
            model.point = 'item:comm:global';
            model.source = 32;
            model.row = 2;

            model.handlePosition = function() {
                return function (text, render) {
                    return ++idx;
                }
            }
            model.handlePrice = function(){
                return function(){
                    var price = ( this.price - 0 ).toFixed(0);

                    return ( price - 0 ) === ( this.price - 0 ) ?
                        price : this.price
                }
            }
            model.handleLink = function(){
                return function(){
                    return this.link
                }
            }
            model.handleTitle = function(){
                return function( txt,render ){
                    return render( this.title )
                }
            }
            model.handleImage = function(){
                return function( txt,render ){
                    return render( this.imgUrl );
                }
            }

            this.renderModel = model;
        });

        suppView.render('after',function( ret ){
            ctrl.find('supply_item').show();
            _supply_bd.find('img').lazyload()
        });

        suppView.request();

        Manager.once('supply.empty',function(){
            suppView.empty();
            ctrl.find('supply_item').hide();
        });
    });

}( Alpha,Manager );;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha,Manager ){
    var global = Function('return this')(),
        _object_ = {},
        _array_ = [],
        _slice_ = _array_.slice,
        _hasOwn_ = _object_.hasOwnProperty,
        extend = Alpha.extend;

    Manager.on('promo.build',function( data ){
        var ctrl = this.ctrl,
            pipe,
            state = Manager.get('filter.state'),
            en_word = encodeURIComponent( Manager.get('word')),
            skw,
            pv = Manager.report.pv;

        if( !(state == 'default') ){
            return;
        }

        pipe = Alpha.Pipe.factory();

        pipe.inject(function( operate ){
            var wordView,
                keyword,
                scene = Manager.Info.get('scene');

            wordView = Alpha.View.factory({
                url: Manager.get('api.word'),
                template:ctrl.find('delete_tpl'),
                dataType: 'jsonp'
            });

            wordView.request('before',function( p ){
                extend(p,{
                    data: {
                        keyword: scene == 'cart'? Manager.get('landData.title'): Manager.Info.get('keyword'),
                        charset: scene == 'cart' ? 'utf-8':'gbk',
                        datatype: 'jsonp'
                    }
                });
            });

            wordView.request('done',function( ret ){
                if( ret.error == '0' ){
                    return this.render(ret);
                }
                this.emit('request.fail');
            });

            wordView.render('after',function(ret,html){
                operate.next(keyword,html );
            })

            wordView.render('format',function( ret ){
                var model = {},
                    data = ret.data,
                    hold,
                    serial,
                    word,
                    key = [],
                    i;

                hold = data.result_enc.split('{|}');
                serial = hold[1].split(',');
                word = hold[0].split('%2C');

                i = serial.length;

                while( i-- ){
                    key[i] = word[ serial[i] ];
                }

                keyword = key.join('%20');

                hold = data.result.split('{|}');
                serial = hold[1].split(',');
                word = hold[0].split(',');

                i = serial.length;

                while( i-- ){
                    key[i] = encodeURIComponent( word[ serial[i] ] );
                }

                skw = key.join('~');

                model.handleWord = function(){
                    return function( txt,render ){
                        var hold = data.result.split('{|}'),
                            serial = hold[1].split(','),
                            word = hold[0].split(','),
                            i = serial.length,
                            len = word.length,
                            title = [],
                            j;

                        while( i-- ){
                            hold = serial[ i ] - 0;
                            j = len;
                            while( j-- ){
                                if( j === hold ){
                                    title[ i ] = '<strong>'+ word[j] +'</strong>'
                                }
                            }
                        }

                        return '给您推荐搜索“'+ title.join('') +'”的结果：'
                    }
                }

                model.handleCommTpl = function(){
                    return function( text,render ){
                        return ctrl.find('page_comm_tpl').html()
                    }
                }

                model.handleFootTpl = function(){
                    return function(){
                        return ctrl.find('delete_foot_tpl').html()
                    }
                }

                this.renderModel = model;
            });

            wordView.request();
        });

        pipe.inject(function( keyword,tpl,operate ){
            var deleView,
                _delete_item = ctrl.find('delete_item');

            deleView = Alpha.View.factory({
                url: Manager.get('api.comm'),
                elem: _delete_item,
                template: $( '<div>' + tpl + '</div>'),
                dataType: 'jsonp'
            });

            deleView.request('before',function(p){
                extend(p,{
                    dataType: 'jsonp',
                    timeout: 5000,
                    data: {
                        PageNum: 1,
                        PageSize: 20,
                        charset: 'gbk',
                        dtype: 'jsonp',
                        g_tk: 12,
                        KeyWord: keyword,
                        OrderStyle: 77,
                        ac: 1,
                        sf: 2007
                    }
                });
            });

            deleView.request('fail',function(){
                pv.empty('site').empty('expo',{
                    defItemnum: 4
                }).empty('search',
                    {
                        extraMsg: 'h5_st_5',
                        defItemnum: 4
                    }
                );

                operate.fail();
            });

            deleView.request('done',function( ret ){
                var each = Alpha.each,
                    res = [],
                    total = ret.data.totalNum,
                    sum = 0;

                if( ret.retCode == '0' ){
                    if( total < 4 ){
                        return this.emit('request.fail');
                    }

                    if( data ){
                        each(data.data.list,function( i,elem ){
                            each( ret.data.list,function( j,el ){
                                if( elem.commId != el.commId ){
                                    res.push( el );
                                    sum++;
                                }
                                if( sum > 4 ){
                                    return false;
                                }
                            });
                            if( sum > 4 ){
                                return false;
                            }
                        });

                        ret.data.list = res;
                    }

                    return ret.data.length < 3 ?
                        this.emit('request.fail') : operate.next( deleView, ret)
                }

                this.emit('reqeust.fail');
            });

            deleView.render('format',function( ret ){
                var m = {},
                    size = Manager.Info.get('size'),
                    data = ret.data,
                    idx = 0,
                    grey = Manager.Info.get('grey');

                m.item = data.list.slice(0,4);
                m.page = 1;
                m.pvid = pv.pvid();
                m.total = data.totalNum;
                m.point = 'item:comm:global';
                m.source = 32;
                m.row = 2;

                m.btnTotal = ret.data.totalNum;
                m.btnLink = (function( query ){
                    var href = [],
                        location = global.location,
                        hasOwn = _hasOwn_;

                    for( var key in query ){
                        if( hasOwn.call( query,key ) ){
                            switch( key ){
                                case 'keyword':
                                    href.push( 'keyword=' + keyword );
                                    continue;
                                case 'globalsearch':
                                    href.push( 'globalsearch=1' );
                                    continue;
                                case 'lpitemtitle':
                                case 'lpitemid':
                                case 'showlpitem':
                                    continue;
                                default:
                                    href.push( key + '=' + query[key] );
                            }
                        }
                    }

                    return location.origin + location.pathname + '?' + href.join('&');
                })( Manager.Info.query );

                m.handlePosition = function(){
                    return function( text,render ){
                        return ++idx;
                    }
                }

                m.handleSale = function(){
                    return function(){
                        var saleNum = this.saleNum - 0;

                        if( saleNum > 1000000 ){
                            saleNum = (Math.round( saleNum / 10000 )).toFixed( 0 ) + '万';
                        }
                        else if( saleNum >= 10000 ){
                            saleNum = ( saleNum / 10000 ).toFixed( 1 ) + '万';
                        }

                        return this.saleNum == '0' ? '' : '<p class="comm_sale_num">销量'+ saleNum +'件</p>';
                    }
                }

                m.handlePrice = function(){
                    return function(){
                        var price = ( this.price - 0 ).toFixed(0);

                        return ( price - 0 ) === ( this.price - 0 ) ?
                            price : this.price
                    }
                }

                m.handleLink = function(){
                    return function(){
                        switch( this.Active_Price_Type ){
                            case '1':
                                return this.paiPianYiLink;
                            default:
                                return this.h5link;
                        }
                    }
                }

                m.handleType = function(){
                    return function(){
                        switch( this.Active_Price_Type ){
                            case '1':
                                return 'ppy';
                            case '3':
                                return 'sg';
                        }
                    }
                }

                m.handleState = function(){
                    return function(){
                        switch ( this.PersonalizedTypeTag ){
                            case '1':
                                return '<i class="icon_bought">已下单</i>';
                            case '2':
                                return '<i class="icon_browsed">已浏览</i> '
                        }
                    }
                }

                m.handlePromo = function(){
                    return function( txt,render ){
                        switch( this.Active_Price_Type ){
                            case '1':
                                return '<i class="icon_expinsive"></i>';
                            case '3':
                                return '<i class="icon_quickbuy"></i>'
                        }
                    }
                }
                m.handleTitle = function(){
                    return function( txt,render ){
                        return render( this['title'] )
                    }
                }
                m.handleImage = function(){
                    return function( txt,render ){
                        return render( this[size] );
                    }
                }

                this.renderModel = m;
            });

            deleView.render('after',function(){
                _delete_item.find('img').lazyload();

                Manager.once('delete.empty',function(){
                    deleView.empty();
                })
            });

            deleView.render('before',function(ret){
                var data = ret.data;

                pv.report('site','page=1',true)
                    .report('search',{
                        curPage: 1,
                        sortType: pv.parse('sort', Manager.get('sortType') ),
                        hitItemnum: data.totalNum,
                        keyword: en_word,
                        defItemnum: 4,
                        skw: skw
                    },{extraMsg: 'h5_st_5'})
                    .report('expo',extend({
                        hitItemnum: data.totalNum,
                        defItemnum: 4
                    },pv.parse('expo',ret.data.list.slice(0,4))));
            })

            deleView.request();
        });

        pipe.on('done',function( view,ret ){
            view.render( ret );
        });

        pipe.inject();
    });

}( Alpha,Manager );;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha,Manager ){
    var global = Function('return this')(),
        extend = Alpha.extend;

    Manager.on('main.init',function( data ){
        var page = this.page || ( this.page = {} );

        page.buildGlobalShop = function(){
            var manager = Manager,
                size,
                word,
                keyword,
                en_word,
                data,
                page,
                pv = manager.report.pv,
                ctrl = manager.ctrl,

                _shop_item = ctrl.find('shop_item'),
                _loading_wrap = ctrl.find('loading_wrap'),
                _load_done = ctrl.find('load_done'),
                _load_timeout = _load_done.clone().attr('id','load_timeout').css('display','block'),
                _global_shop_empty = ctrl.find('global_shop_empty'),
                _filter_empty_shop = ctrl.find('filter_empty_shop'),

            keyword = manager.Info.get('keyword');
            word = manager.get('word');
            en_word = encodeURIComponent( word );

            _global_shop_empty.find('em').text(word);
            _filter_empty_shop.find('em').text(word);

            if( manager.get('ban') == '1' ){
                _shop_item.append( _global_shop_empty.show() );
                _loading_wrap.hide();
                return false;
            }

            page = Alpha.Page.factory({
                elem: _shop_item,
                template: ctrl.find('page_shop_tpl'),
                url:manager.get('api.shop'),
                dataType: 'jsonp'
            });

            page.set('locked',false);

            page.request('before',function( p ){
                extend( p,{
                    dataType: 'jsonp',
                    timeout: 5000,
                    data: extend({},manager.get('page.data'),{
                        charset: 'gbk',
                        g_tk: 12,
                        KeyWord: keyword,
                        dtype: 'jsonp',
                        PageNum: this.current,
                        PageSize: size = 10,
                        OrderStyle: manager.get('sortType'),
                        PageType: 4,
                        sf: 1004
                    })
                });

                _loading_wrap.show();
                _load_timeout.hide();
                ctrl.set('locked',true);
            });

            page.request('done',function( ret ){
                var data = ret.data,
                    total = data.totalNum;

                ret.retCode == '0' ?
                    data.shopList.length > 0 ?
                        this.render( ret )
                            .emit('update',ret)
                        : total > 0 ? this.emit('end',ret) : this.emit('none',ret)
                    : this.emit('request.fail',ret);
            });

            page.on('update',function( ret ){
                var sum = this.current * size,
                    total = ret.data.totalNum;

                sum >= total ?
                    this.emit('end') : this.emit('finish');
            });

            page.request('fail',function( err ){
                var extra,
                    state = manager.get('filter.state'),
                    method;

                if( state == 'change' ){
                    extra = extra || [];
                    extra.push( 'h5_st_13' );
                }

                //this.set('locked',true);
                ctrl.set('locked',false);

                pv.empty('search',extra ? {extraMsg:extra.join('~')} : '');

                this.current > 1 ?
                    method = 'append' : method = 'html';

                if( this.current > 1 ){
                    _load_timeout.find('p').html('加载失败，请重新加载...');
                }
                else{
                    _load_timeout.find('p').html('加载失败，请重新刷新...');
                }

                this.current -= 1;

                _loading_wrap.hide();
                _shop_item [method ]( _load_timeout.show() );
            });

            page.render('before',function( ret ){
                var extra;

                if( manager.get('filter.state') == 'change' ){
                    extra = extra || [];
                    extra.push( 'h5_st_13' );
                }

                pv.report('site','page=' + this.current +'&issearchshop=1')
                    .report('search',{
                        curPage: this.current,
                        hitItemnum: ret.data.totalNum,
                        keyword: en_word
                    },extra ? {extraMsg:extra.join('~')} : '');
            });

            page.render('after',function(){
                _shop_item.find('img').lazyload();
                Manager.emit('share.update');
            });

            page.render('format',function( ret ){
                var model = {},
                    idx = 0,
                    data = ret.data;

                model.item = data.shopList;
                model.pvid = pv.pvid();
                model.page = this.current;
                model.total = data.totalNum;
                model.point = 'item:shop';
                model.source = 31;
                model.row = 1;

                model.handlePosition = function(){
                    return function( text,render ){
                        return ++idx;
                    }
                }
                model.handleName = function(){
                    return function( txt,render ){
                        return render( this['shopName'] );
                    }
                }
                model.handleLoc = function(){
                    return function( txt,render ){
                        return this['shopAddress'] == '未选择' ? '' : '<p class="shop_loc">所在地：'+ render( this['shopAddress'] ) +'</p>';
                    }
                }
                model.handleCate = function(){
                    return function( txt,render ){
                        return this['mainCategory'] == '未填写' ? '' : '<p class="shop_cate">所有类目：'+ render( this['mainCategory'] ) +'</p>';
                    }
                }

                this.renderModel = model;
            });

            page.on('none',function(){
                var extra,
                    state = manager.get('filter.state');

                if( state == 'change' ){
                    extra = extra || [];
                    extra.push( 'h5_st_13' );
                }

                this.set('locked',true);
                ctrl.set('locked',false);

                pv.empty('search',extra ? {extraMsg:extra.join('~')} : '');

                _loading_wrap.hide();
                _shop_item.html( state == 'change' ? _filter_empty_shop.show():_global_shop_empty.show() );
            });
            page.on('end',function(){
                this.set('locked',true);
                ctrl.set('locked',false);

                _loading_wrap.hide();
                _shop_item.append( _load_done.show() );
            });
            page.on('finish',function(){
                _loading_wrap.hide();
                ctrl.set('locked',false);
                Manager.emit('tips.shop.comm');
            });

            return page;
        }

    });

}( Alpha,Manager );;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha,Manager ){
    'use strict';
    var global = Function('return this')(),
        extend = Alpha.extend;

    Manager.on('main.init',function( data ) {
        var scene = Manager.Info.get('scene'),
            page = this.page || ( this.page = {} );

        page.build = function( type ){
            switch( type ){
                case 'shop_comm':
                    return this.buildShopComm();
                case 'landing_page':
                case 'cart':
                case 'global_comm':
                    return this.buildGlobalComm();
                case 'global_shop':
                    return this.buildGlobalShop();
            }
        }

       Manager.emit('main.build');
    });

}( Alpha,Manager );
;/**
 * Created by weiyanhai on 14/12/29.
 */
;!function( Alpha,Manager ){
    var global = Function('return this')(),
        _array_= [],
        _slice_ = _array_.slice,
        extend = Alpha.extend;

    Manager.on('main.init',function( data ){
        var api = Manager.get('api.piece'),
            link,
            word = this.get('word'),
            en_word = encodeURIComponent( word),
            pv = Manager.report.piece,
            cache,
            size = 6,
            total = 0,
            index = 0,
            end = false;

        link = (function( query ){
            var hasOwn = {}.hasOwnProperty,
                location = global.location,
                link = location.origin + location.pathname + '?',
                ret = [];

            for( var k in query ){
                if( hasOwn.call( query,k ) ){
                    if( k == 'keyword' ){
                        continue;
                    }
                    ret.push(k + '=' + query[k])
                }
            }

            return link + ret.join('&') + '&ptag=20511.3.3';

        })( Manager.Info.query );

        Manager.on('piece.build',function( dom,keyword,word ){
            var manager = Manager,
                ctrl = manager.ctrl,
                piece;

            if( manager.Info.get('oldScene') ){
                return;
            }

            piece = Alpha.View.factory({
                url: Manager.get('api.piece'),
                elem: dom,
                template: ctrl.find('piece_tpl'),
                dataType: 'jsonp'
            });

            if( cache ){
                piece.data = cache;
            }

            piece.request('done',function( ret ){
                var lable = ret.label[ word ];

                lable ?
                    lable.value.length > 0 ?
                        this.render( ret,function(elem,html,model){
                            elem.after( html );
                        }) : this.emit('request.fail')
                    : this.emit('request.fail');
            });

            piece.request('fail',function(){
               end = true;
            });

            piece.render('before',function( ret ){
                var value = ret.label[ word].value,
                    mod,
                    slice = _slice_,
                    value = ret.label[ word].value,
                    offset = size * index,
                    each = Alpha.each;

                if( !cache ){
                    cache = ret;
                    mod = value.length / size;
                    mod == mod.toFixed(3) ?
                        total = parseInt( mod )
                        : total = parseInt( mod ) + 1;
                }

                ret.Piece = total < 2 ?
                    slice.call(value,0,value.length) :
                    slice.call(value, offset,offset + size );

                each( ret.Piece,function(i,el){
                    el.word = el[ 1 ];
                    el.commId = encodeURIComponent( el[1] );
                });

                index++;

                if( index >= total ){
                    end = true;
                }


            });

            piece.render('before',function( ret ){
                var total = ret.Piece.length;

                pv.report('site','page=' + index,true)
                    .report('search',{
                        curPage: index,
                        sortType: pv.parse('sort', Manager.get('sortType') ),
                        hitItemnum: total,
                        defItemnum:6,
                        keyword: en_word,
                        extraMsg: 'h5_inword'
                    })
                    .report('expo',extend({
                        hitItemnum: total,
                        extraMsg: 'h5_inword',
                        defItemnum:6
                    },pv.parse('expo',ret.Piece)));
            });

            piece.render('format',function( ret ){
                var model = {},
                    l = link,
                    idx = 0;

                model.item = ret.Piece;

                model.handlePosition = function(){
                    return function(){
                        return ++idx;
                    }
                }
                model.handleLink = function(){
                    return function(){
                        return l + '&keyword=' + word + ' ' + this[1];
                    }
                }

                model.handleName = function(){
                    return function(){
                        return this[1];
                    }
                }

                this.renderModel = model;
            });


            piece.request('before',function( p ){
                extend(p,{
                    data:{
                        KeyWord: word,
                        LabelCmd: 'r',
                        charset: 'utf-8'
                    }
                });
            });

            if( !end ){
                piece.request();
            }
        });
    });

}( Alpha,Manager );