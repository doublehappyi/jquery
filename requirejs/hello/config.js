/**
 * Created by db on 15-3-15.
 */
requirejs.config({
    baseUrl: "../",
    paths: {
        'jquery': ["lib/jquery"],
        'underscore': ['lib/underscore'],
        'backbone': ['lib/backbone']
    },
    shim: {
        'backbone': {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'underscore': {
            exports: '_'
        }
    }
});